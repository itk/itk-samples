package uk.nhs.interoperability.client.samples.notification;

import static org.junit.Assert.*;

import org.junit.Test;

public class NotificationTest {

	@Test
	public void testSerialise() {
		Notification n = new Notification();
		n.setMessageId("NOTIFICATIONTEST");
		n.setEffectiveTime("20121127151500+0000");
		n.setDateOfBirth("20020831");
		n.setSenderOrg("Organisation1");
		n.setRecipientOrg("Organisation2");
		n.setRecipientAddress("Leeds, LS1 4HY");
		n.setPatientName("Joe Bloggs");
		n.setPatientAddress("Cleckheaton, BD19 3RH");
		n.setNHSNumber("1234556789");
		n.setEventTypeCode("01");
		n.setEventTypeDesc("DocumentEvent");
		n.setContactName("Fred Bloggs");
		n.setContactAddress("Bradford, BD1 1HY");
		n.setContactOrg("Organisation3");
		System.out.println(n.serialise());
	}

}
