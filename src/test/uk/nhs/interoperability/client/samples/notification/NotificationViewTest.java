package uk.nhs.interoperability.client.samples.notification;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.nhs.interoperability.client.samples.testutils.FileLoader;
import uk.nhs.interoperability.client.samples.testutils.FileWriter;

public class NotificationViewTest {

	@Test
	public void testDoProcess() {
		NotificationView nv = new NotificationView();
		FileLoader fl = new FileLoader();
		
		NotificationStore.putNotification("NOTIFICATION", fl.loadFileFromClasspath("NotificationExample.xml"));
		
		String notification = nv.process("NOTIFICATION");
		System.out.println(notification);
		//FileWriter.writeFile("c:\\Temp\\output.htm", notification.getBytes());
	}

}
