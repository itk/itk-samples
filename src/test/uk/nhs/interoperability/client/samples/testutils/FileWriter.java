package uk.nhs.interoperability.client.samples.testutils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Convenience class to write the content of a byte array to a file
 * @author Adam Hatherly
 */
public class FileWriter {

    /**
     * @param filename Filename to write data into
     * @param data array of bytes to write to specified file
     * @return true if successful, false otherwise
     */
    public static boolean writeFile(String filename, byte[] data) {
        boolean success = false;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(filename));
            for (int n=0; n<data.length; n++) {
                fos.write(data[n]);
            }
            success = true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try { fos.close(); } catch (IOException ex) {}
        }
        return success;
    }
}
