package uk.nhs.interoperability.client.samples.testutils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Convenience class to load a file from a given filename and return the
 * content as a string.
 * @author Adam Hatherly
 */
public class FileLoader {

	
	public String loadFileFromClasspath(final String filename) {
		String content = null;
		InputStream in = null;
		try {
			in = this.getClass().getClassLoader().getResourceAsStream(filename);
			ByteArrayOutputStream bOutStream = new ByteArrayOutputStream();
	        int c = -1;
	        while ((c = in.read()) > -1) {
	            bOutStream.write(c);
	        }
	        content = bOutStream.toString();
		} catch (IOException ex) {
            Logger.getLogger(FileLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) { }
        }
        return content;
	}
	
    /**
     * @param filename Filename to load content from
     * @return String containing content of specified file
     */
    public static String loadFile(final String filename) {
        String content = null;
        FileReader fr = null;
        try {
            fr = new FileReader(new File(filename));
            ByteArrayOutputStream bOutStream = new ByteArrayOutputStream();
            int c = -1;
            while ((c = fr.read()) > -1) {
                bOutStream.write(c);
            }
            content = bOutStream.toString();
        } catch (IOException ex) {
            Logger.getLogger(FileLoader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fr.close();
            } catch (IOException ex) { }
        }
        return content;
    }
}
