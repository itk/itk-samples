function loadSample(httpRequest){
    if (httpRequest.readyState == 4){
        // everything is good, the response is received
        if ((httpRequest.status == 200) || (httpRequest.status == 0)){
			var docText = document.getElementById("docText");
			docText.value = httpRequest.responseText;
        }else if(httpRequest.status == 404){
        	alert('Example message not found');
        } else {
            alert('There was a problem with the request. ' + httpRequest.status + httpRequest.responseText);
        }
    }
}

function getSample( sampleURL ){
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() { loadSample(httpRequest); };
    httpRequest.open("GET", sampleURL, true);
    httpRequest.send(null);
}

function setServiceId(serviceId){
	var sifield = document.getElementById("serviceId");
	sifield.value = serviceId;
	
}