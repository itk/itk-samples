<!-- Note this was copied/refactored from itk-cloudharness module on 14/12/2012 by MOS-->
<xsl:stylesheet version="2.0" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:hl7="urn:hl7-org:v3"
        exclude-result-prefixes="xs hl7">
        
    <xsl:output method="xml"/>
    
    <xsl:param name="response-msg-id"/>
    
    <xsl:template match="/">
		<getNHSNumberResponse-v1-0 xmlns="urn:hl7-org:v3" moodCode="EVN" classCode="OBS">
			<id root="{$response-msg-id}" />
			<code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.284" code="getNHSNumberResponse-v1-0" />
			<value codeSystem="2.16.840.1.113883.2.1.3.2.4.17.285" code="SMSP-0000" />
			<subject typeCode="SBJ">
				<patient classCode="PAT">
					<id root="2.16.840.1.113883.2.1.4.1" extension="1122334455" />
					<id root="2.16.840.1.113883.2.1.3.2.4.18.24" extension="{//hl7:Person.LocalIdentifier/hl7:value/@extension}" />
				</patient>
			</subject>
		</getNHSNumberResponse-v1-0>
    </xsl:template>
    
</xsl:stylesheet>    