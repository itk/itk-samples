<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xs fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/">

		<wsnt:Notify xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2">
			<wsnt:NotificationMessage>
				<wsnt:Message>

					<EventNotification xmlns:npfitlc="NPFIT:HL7:Localisation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:hl7-org:v3" classCode="INFRM" moodCode="EVN">
						<code code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.436" displayName="Event Notification"/>
						<!-- EffectiveTime -->
						<effectiveTime>
							<xsl:attribute name="value">
								<xsl:value-of select="Message/EffectiveTime"/>
							</xsl:attribute>
						</effectiveTime>
	
						<!-- MessageId -->
						<id>
							<xsl:attribute name="root">
								<xsl:value-of select="Message/MessageId"/>
							</xsl:attribute>
						</id>

						<!-- Author -->
						<author typeCode="AUT">
							<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145231GB01#OriginatingSystem"/>
							<COCD_TP145231GB01.OriginatingSystem classCode="ASSIGNED">
								<code code="OSY" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.205" displayName="Originating System"/>
								<id root="2.16.840.1.113883.2.1.3.2.4.18.36" extension="112YT" assigningAuthorityName="RA8:St Elsewhere's Hospital"/>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145231GB01#OriginatingSystem"/>
								<representedOrganization classCode="ORG" determinerCode="INSTANCE">
									<id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="RA8"/>
									<!-- SenderOrg -->
									<name><xsl:value-of select="Message/SenderOrg"/></name>
									<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145231GB01#representedOrganization"/>
								</representedOrganization>
							</COCD_TP145231GB01.OriginatingSystem>
						</author>
						
						<!-- Recpipient -->
						<informationRecipient typeCode="IRCP">
							<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145218GB01#IntendedRecipient"/>
							<COCD_TP145218GB01.IntendedRecipient classCode="ASSIGNED">
								<!-- Recpipient Address -->	
								<addr><xsl:value-of select="Message/RecipientAddress"/></addr>
								<code code="NR0260" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124" displayName="General Medical Practitioner"/>
								<telecom value="tel:01132111112"/>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145218GB01#IntendedRecipient"/>
								<representedOrganization classCode="ORG" determinerCode="INSTANCE">
									<!-- Recpipient Org -->	
									<name><xsl:value-of select="Message/RecipientOrg"/></name>
									<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145218GB01#representedOrganization"/>
								</representedOrganization>
							</COCD_TP145218GB01.IntendedRecipient>
						</informationRecipient>
						
						<!-- Patient -->
						<recordTarget typeCode="RCT">
							<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145216GB01#PatientRole"/>
							<COCD_TP145216GB01.PatientRole classCode="PAT">
								<!-- PatientAddress -->
								<addr><xsl:value-of select="Message/PatientAddress"/></addr>
								<!-- NHSNumber -->
								<id root="2.16.840.1.113883.2.1.4.1">
									<xsl:attribute name="extension">
										<xsl:value-of select="Message/NHSNumber"/>
									</xsl:attribute>
								</id>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145216GB01#PatientRole"/>
								<patientPatient classCode="PSN" determinerCode="INSTANCE">
									<administrativeGenderCode codeSystem="2.16.840.1.113883.2.1.3.2.4.16.25" displayName="Male" code="1"/>	
									<!-- DateOfBirth -->
									<birthTime>
										<xsl:attribute name="value">
											<xsl:value-of select="Message/DateOfBirth"/>
										</xsl:attribute>
									</birthTime>
									<!-- PatientName -->
									<name><xsl:value-of select="Message/PatientName"/></name>
									<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145216GB01#patientPatient"/>
								</patientPatient>
							</COCD_TP145216GB01.PatientRole>
						</recordTarget>

						<!-- Performer (aka Contact person) -->
						<performer typeCode="PRF">
							<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145217GB01#ContactPerson"/>
							<COCD_TP145217GB01.ContactPerson classCode="ASSIGNED">
								<addr><xsl:value-of select="Message/ContactAddress"/></addr>
								<code code="NR0050" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124" displayName="Consultant"/>
								<telecom value="tel:01132111111"/>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#ContactPerson"/>
								<assignedPerson classCode="PSN" determinerCode="INSTANCE">
									<name><xsl:value-of select="Message/ContactName"/></name>
									<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#assignedPerson"/>
								</assignedPerson>
								<representedOrganization classCode="ORG" determinerCode="INSTANCE">
									<id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="RA8"/>
									<name><xsl:value-of select="Message/ContactOrg"/></name>
									<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#representedOrganization"/>
								</representedOrganization>
							</COCD_TP145217GB01.ContactPerson>
						</performer>
						
						<!-- Event -->
						<component typeCode="COMP">
							<eventInformation classCode="INFRM" moodCode="EVN">
								<!-- Event Type -->	
								<code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.437">
									<!-- EventTypeCode -->
									<xsl:attribute name="code">
										<xsl:value-of select="Message/EventTypeCode"/>
									</xsl:attribute>
									<!-- EventTypeDesc -->
									<xsl:attribute name="displayName">
										<xsl:value-of select="Message/EventTypeDesc"/>
									</xsl:attribute>
								</code>
								<!-- EffectiveTime -->
								<effectiveTime>
									<xsl:attribute name="value">
										<xsl:value-of select="Message/EffectiveTime"/>
									</xsl:attribute>
								</effectiveTime>
								<id root="1A97FCE1-389F-11E2-81C1-0800200C9A66"/>
								
								<!-- Document Details -->
								<component typeCode="COMP">
									<documentDetails classCode="DOCCLIN" moodCode="EVN">
										<code code="861421000000109" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.438" displayName="End of Life Care Coordination Summary"/>
										<id root="2.16.840.1.113883.2.1.3.2.4.18.21" extension="28BEE6CC-28F4-11E2-B493-B2916188709B"/>
										<setId root="2.16.840.1.113883.2.1.3.2.4.18.45" extension="2E0A4036-28F4-11E2-A045-B3916188709B"/>
									</documentDetails>
								</component>
								<component1 typeCode="COMP">
									<uRL classCode="OBS" moodCode="EVN">
										<code code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.439" displayName ="URL for remote system access"/>
										<value>./index.html</value>
									</uRL>
								</component1>
								<component1 typeCode="COMP">
									<uRL classCode="OBS" moodCode="EVN">
										<code code="02" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.439" displayName ="URL for document access (latest version)"/>
										<value>./documents/EndOfLifeCareCoordinationSummary.htm</value>
									</uRL>                                                       
								</component1>
								<component2 typeCode="COMP">
									<documentFormat classCode="OBS" moodCode="EVN">
										<code code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Document Format"/>
										<value code="text/xml" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.441"/>
									</documentFormat>
								</component2>
								<component3 typeCode="COMP">
									<profileID classCode="OBS" moodCode="EVN">
										<code code="02" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Profile ID"/>
										<value root="2.16.840.1.113883.2.1.3.2.4.18.28" extension="urn:nhs-en:profile:EndofLifeRecordCDADocument-v1-0"/>
									</profileID>
								</component3>
								<component4 typeCode="COMP">
									<eventSubtype classCode="OBS" moodCode="EVN">
										<code code="03" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Document Subtype"/>
										<value code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.451" displayName="Document Created"/>
									</eventSubtype>
								</component4>
								
								<!-- Contact Person -->
								<performer typeCode="PRF">
									<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145217GB01#ContactPerson"/>
									<COCD_TP145217GB01.ContactPerson classCode="ASSIGNED">
										<!-- ContactAddress -->
										<addr><xsl:value-of select="Message/ContactAddress"/></addr>
										<code code="NR0050" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124" displayName="Consultant"/>
										<telecom value="tel:01132111111"/>
										<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#ContactPerson"/>
										<assignedPerson classCode="PSN" determinerCode="INSTANCE">
											<!-- ContactName -->
											<name><xsl:value-of select="Message/ContactName"/></name>
											<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#assignedPerson"/>
										</assignedPerson>
										<representedOrganization classCode="ORG" determinerCode="INSTANCE">
											<id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="RA8"/>
											<!-- ContactOrg -->
											<name><xsl:value-of select="Message/ContactOrg"/></name>
											<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#representedOrganization"/>
										</representedOrganization>
									</COCD_TP145217GB01.ContactPerson>
								</performer>
							</eventInformation>
						</component>
						
					</EventNotification>



				</wsnt:Message>
			</wsnt:NotificationMessage>
		</wsnt:Notify>


	</xsl:template>
</xsl:stylesheet>
