<!-- Note this was copied/refactored from itk-cloudharness module on 14/12/2012 by MOS-->
<xsl:stylesheet version="2.0" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:v3="urn:hl7-org:v3"
        exclude-result-prefixes="xs">
        
    <xsl:output method="xml"/>
    
    <xsl:param name="creation-time"/>
    <xsl:param name="response-msg-id"/>
    <xsl:param name="from-address"/>
    <xsl:param name="to-address"/>
    
    <xsl:template match="/">
		<v3:BusinessResponseMessage>
		    <v3:id root="{$response-msg-id}" />
		    <v3:creationTime value="{$creation-time}" />
		    <v3:interactionId root="2.16.840.1.113883.2.1.3.2.4.12" extension="urn:nhs-itk:interaction:ITKBusinessAcknowledgement-v1-0" />
		    <v3:acknowledgedBy3 typeCode="AA">
		        <v3:conveyingTransmission>
		            <v3:id root="7D6D9D40-AE1A-11DB-97F9-B18E1E0994AA" />
		        </v3:conveyingTransmission>
		        <v3:acknowledgementDetail typeCode="IF">
		            <v3:code code="LOCAL1234567" codeSystem="1.2.3.4.5.6.7.8.9.0"
		                displayName="For Information display name" />
		        </v3:acknowledgementDetail>
		    </v3:acknowledgedBy3>
		    <v3:communicationFunction1 typeCode="SND">
		        <v3:entity classCode="ENT" determinerCode="INSTANCE">
		            <v3:id root="2.16.840.1.113883.2.1.3.2.4.18.22" extension="{$from-address}" />
		        </v3:entity>
		    </v3:communicationFunction1>
		    <v3:communicationFunction typeCode="RCV">
		        <v3:entity classCode="ENT" determinerCode="INSTANCE">
		            <v3:id root="2.16.840.1.113883.2.1.3.2.4.18.22" extension="{$to-address}" />
		        </v3:entity>
		    </v3:communicationFunction>
		    <v3:controlActEvent1 classCode="CACT" moodCode="EVN">
		        <v3:author typeCode="AUT" contextControlCode="OP">
		            <v3:COCD_TP145207GB01.AssignedAuthorDevice
		                classCode="ASSIGNED">
		                <v3:id root="2.16.840.1.113883.2.1.3.2.4.18.24" extension="DEVICEID001" />
		                <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                    extension="COCD_TP145207GB01#AssignedAuthorDevice" />
		                <v3:assignedAuthoringDevice classCode="DEV"
		                    determinerCode="INSTANCE">
		                    <v3:manufacturerModelName>LOCALManufacturerModelRef
		                    </v3:manufacturerModelName>
		                    <v3:softwareName>LOCALSoftwareName</v3:softwareName>
		                    <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                        extension="COCD_TP145207GB01#assignedAuthoringDevice" />
		                </v3:assignedAuthoringDevice>
		                <v3:representedOrganization classCode="ORG"
		                    determinerCode="INSTANCE">
		                    <v3:id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="Y896543" />
		                    <v3:name>Anytown NHS Trust</v3:name>
		                    <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                        extension="COCD_TP145207GB01#representedOrganization" />
		                </v3:representedOrganization>
		            </v3:COCD_TP145207GB01.AssignedAuthorDevice>
		        </v3:author>
		    </v3:controlActEvent1>
		    <v3:acknowledgedBy2 typeCode="AA">
		        <v3:conveyingTransmission>
		            <v3:id root="7D6D9D40-AE1A-11DB-97FF-B18E1E0994AB" />
		            <v3:controlActEvent classCode="CACT" moodCode="EVN">
		                <v3:author typeCode="AUT">
		                    <v3:COCD_TP145200GB01.AssignedAuthor
		                        classCode="ASSIGNED">
		                        <v3:code code="NR0080" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124"
		                            displayName="Staff Grade" />
		                        <v3:id root="2.16.840.1.113883.2.1.3.2.4.18.24" extension="12367890" />
		                        <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                            extension="COCD_TP145200GB01#AssignedAuthor" />
		                        <v3:assignedPerson classCode="PSN"
		                            determinerCode="INSTANCE">
		                            <v3:name>Jane Bristow</v3:name>
		                            <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                                extension="COCD_TP145200GB01#assignedPerson" />
		                        </v3:assignedPerson>
		                        <v3:representedOrganization classCode="ORG"
		                            determinerCode="INSTANCE">
		                            <v3:id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="Y896543" />
		                            <v3:name>Anytown NHS Trust</v3:name>
		                            <v3:templateId root="2.16.840.1.113883.2.1.3.2.4.18.2"
		                                extension="COCD_TP145200GB01#representedOrganization" />
		                        </v3:representedOrganization>
		                    </v3:COCD_TP145200GB01.AssignedAuthor>
		                </v3:author>
		                <v3:reason typeCode="RSON">
		                    <v3:detectedIssueEvent classCode="ALRT"
		                        moodCode="EVN">
		                        <v3:text>Further business details relating to payload response
		                        </v3:text>
		                    </v3:detectedIssueEvent>
		                </v3:reason>
		                <v3:reason typeCode="RSON">
		                    <v3:detectedIssueEvent classCode="ALRT"
		                        moodCode="EVN">
		                        <v3:code code="389" codeSystem="2.16.840.1.113883.2.1.3.2.4.18.30.180999"
		                            displayName="Warning:Unexpected code; Render only">
		                            <v3:originalText>Warning: Rendering only</v3:originalText>
		                            <v3:qualifier code="WG"
		                                codeSystem="2.16.840.1.113883.2.1.3.2.4.17.317" />
		                        </v3:code>
		                    </v3:detectedIssueEvent>
		                </v3:reason>
		            </v3:controlActEvent>
		        </v3:conveyingTransmission>
		        <v3:acknowledgementDetail typeCode="IF">
		            <v3:code code="LOCAL678924" displayName="FURTHER INFO" />
		        </v3:acknowledgementDetail>
		    </v3:acknowledgedBy2>
		</v3:BusinessResponseMessage>
				
    </xsl:template>
    
</xsl:stylesheet>    