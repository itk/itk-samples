<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n1="urn:hl7-org:v3"
				xmlns:npfitlc="NPFIT:HL7:Localisation" xmlns:n2="urn:hl7-org:v3/meta/voc" xmlns:voc="urn:hl7-org:v3/voc"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wsnt="http://docs.oasis-open.org/wsn/b-2" exclude-result-prefixes="n1 n2 voc npfitlc xsi wsnt">
	<xsl:output method="html" indent="yes" version="4.01" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"/>
	
	<!-- Notification -->
	
	<xsl:template match="/wsnt:Notify/wsnt:NotificationMessage/wsnt:Message">
		<html>
			<head>
				<title>
					Notification
				</title>
				<style type="text/css">
						body { color: #000000; font-size: 10pt; line-height: normal; font-family: Verdana, Arial, sans-serif; margin: 10px; }
						a { color: #0099ff; text-decoration: none }
						.input { color: #003366; font-size: 10pt; font-family: Verdana, Arial, sans-serif; background-color: #ffffff; border: solid 1px }
						div.titlebar { background-color: #eeeeff; border: 1px solid #000000; padding: 3px; margin-bottom: 20px;}
						div.doctitle { font-size: 14pt; font-weight: bold; margin-bottom: 10px;}
						div.docsubtitle { padding-top: 10px; font-size: 14pt; font-weight: bold; margin-bottom: 10px;}
						div.header { font-size: 8pt; margin-bottom: 30px; border: 1px solid #000000; background-color: #ffffee;}
						div.footer { font-size: 8pt; margin-top: 30px; border: 1px solid #000000; background-color: #ffffee;}
						p {margin-top: 2px; margin-bottom: 6px;}
						h1 { font-size: 14pt; font-weight: bold; color: #000000; margin-top: 20px; margin-bottom: 10px; border-bottom: 1px solid #CCCCCC;}
						h2 { font-size: 12pt; font-weight: bold; color: #000000; margin-top: 20px; margin-bottom: 6px; }
						h3 { font-size: 12pt; font-weight: normal; color: #000000; margin-top: 15px; margin-bottom: 6px; }
						h4 { font-size: 10pt; font-weight: bold; text-decoration: underline; color: #000000; margin-top: 6px; margin-bottom: 6px; }
						h5 { font-size: 10pt; font-weight: normal; text-decoration: underline;  color: #000000; margin-top: 4px; margin-bottom: 4px; }
						h6 { font-size: 10pt; font-weight: normal; color: #000000; margin-top: 2px; margin-bottom: 2px; }
						table { border: 1px solid #000000; }
						th.default {padding: 3px; color: #000000; background-color: #dddddd; text-align: left;}
						th {padding: 3px; color: #000000; background-color: #dddddd;}
						td {padding: 3px; background-color: #eeeeee;}
						table.titlebar { border: 0px; background-color: #eeeeff; }
						td.titlebar {color: #000000; background-color: #eeeeff; font-weight: bold; }
						th.titlebar {color: #000000; background-color: #eeeeff; font-weight: normal; font-style: italic; text-align: left;}
						table.header { border: 0px; background-color: #ffffee; }
						td.header {color: #000000; background-color: #ffffee; font-weight: bold;}
						th.header {color: #000000; background-color: #ffffee; font-weight: normal; text-align: left; font-style:italic;}
						/*Classes below map to CDA styleCodes*/
						.Bold {font-weight: bold;}
						.Underline {text-decoration:underline;}
						.Italics {font-style:italic;}
						.Emphasis {font-style:italic;}
						.Rrule {border-right: 1px solid black;}
						.Lrule {border-left: 1px solid black;}
						.Toprule {border-top: 1px solid black;}
						.Botrule {border-right: 1px solid black;}
						/*Banner styles*/
						div.banner { font-size: 8pt; margin-bottom: 30px; border: 1px solid #000000; background-color: #ffffee;}
						div.banner TABLE { border: 0px; background-color: #ffffee; font-weight: bold; }
						div.banner TD { background-color: #ffffee; vertical-align: top; padding-right: 1em;}
						div.banner TABLE P {margin: 0;}
						.label {font-style:italic; font-weight: normal;}
				</style>
			</head>
			<body>	
				<xsl:apply-templates select="n1:EventNotification"/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="n1:EventNotification">
		<xsl:call-template name="patientBanner"/>
		<xsl:call-template name="header"/>
		<xsl:apply-templates select="n1:component/n1:eventInformation"/>
	</xsl:template>
	
	<!-- Patient Banner as per CUI guidelines -->
	<xsl:template name="patientBanner">
		<div id="patientBanner" class="banner">
		<table>
			<tbody>
				<tr>
					<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:name">
						<td>
							<xsl:call-template name="cuiName">
								<xsl:with-param name="name" select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:name"/>
							</xsl:call-template>
						</td>
					</xsl:if>
					<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:birthTime">
						<td align="right">
							<span class="label"> Born </span>
							<xsl:call-template name="formatDate">
								<xsl:with-param name="date" select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:birthTime/@value"/>
							</xsl:call-template>
						</td>
					</xsl:if>
					<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:administrativeGenderCode">
						<td align="right">
							<span class="label"> Gender </span>
							<xsl:value-of select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:patientPatient/n1:administrativeGenderCode/@displayName"/>
						</td>
					</xsl:if>
					<td>
						<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id/@root='2.16.840.1.113883.2.1.4.1'">
						<!-- Verified NHS Number -->
							<span class="label"> NHS No. </span>
							<xsl:call-template name="cuiNHSNo">
								<xsl:with-param name="nhsNo" select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id[@root='2.16.840.1.113883.2.1.4.1']/@extension"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id/@root='2.16.840.1.113883.2.1.3.2.4.18.23'">
						<!-- Unverified NHS Number -->
							<span class="label"> Unverified NHS No. </span>
							<xsl:call-template name="cuiNHSNo">
								<xsl:with-param name="nhsNo" 
									select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id[@root='2.16.840.1.113883.2.1.3.2.4.18.23']/@extension"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:for-each select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id[@root='2.16.840.1.113883.2.1.3.2.4.18.24']">
						<!-- Local ID -->
							<span class="label"> Local Patient ID </span>
							<xsl:value-of select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:id[@root='2.16.840.1.113883.2.1.3.2.4.18.24']/@extension"/>
						</xsl:for-each>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="header">
			<tbody>
				<tr>
					<td>
						<xsl:for-each select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr">
							<p class="label">
							<xsl:choose>
							<!-- TODO: enumerate all values -->
								<xsl:when test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/@use='TMP'">Temporary Address</xsl:when>
								<xsl:when test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/@use='H'">Home Address</xsl:when>
								<xsl:when test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/@use='WP'">Work Address</xsl:when>
								<xsl:when test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/@use='PST'">Postal Address</xsl:when>
								<xsl:otherwise>Address</xsl:otherwise>
							</xsl:choose>
							</p>
							<p>
								<xsl:for-each select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/n1:streetAddressLine">
									<xsl:value-of select="."/><br/>
								</xsl:for-each>
								<xsl:value-of select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/n1:city"/><br/>
								<xsl:value-of select="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:addr/n1:postalCode"/>
								<!-- TODO: more elements possible here -->
								<!-- AH Added to support unstructured addresses -->
								<xsl:if test="count(descendant::*) = 0">
									<xsl:value-of select="."/>
								</xsl:if>
							</p>
						</xsl:for-each>
					</td>
					<td>
						<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom/@use='H'">
							<span class="label">Home </span>
							<xsl:value-of select="substring-after(./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom[@use='H']/@value, ':')"/><br />
						</xsl:if>
						<xsl:if test="./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom/@use='WP'">
							<span class="label">Work </span>
							<xsl:value-of select="substring-after(./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom[@use='WP']/@value, ':')"/><br/>
						</xsl:if>
						<xsl:if test="contains(./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom/@value, 'mailto')">
							<span class="label">Email </span>
							<xsl:value-of select="substring-after(./n1:recordTarget/n1:COCD_TP145216GB01.PatientRole/n1:telecom[contains(@value, 'mailto')]/@value, ':')"/><br/>
						</xsl:if>
						<!-- TODO: More telecom types -->
					</td>
				</tr>
			</tbody>
		</table>
		</div>
	</xsl:template>

	<!-- Get a Name  -->
	<xsl:template name="getName">
		<xsl:param name="name"/>
		<xsl:choose>
			<xsl:when test="$name/n1:family">
				<xsl:if test="$name/n1:prefix">
					<xsl:value-of select="$name/n1:prefix"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="$name/n1:given"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$name/n1:family"/>
				<xsl:if test="$name/n1:suffix">
					<xsl:text> </xsl:text>
					<xsl:value-of select="$name/n1:suffix"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Format name as per CUI guidance  -->
	<xsl:template name="cuiName">
		<xsl:param name="name"/>
		<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
		<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
		<xsl:choose>
			<xsl:when test="$name/n1:family">
				<xsl:value-of select="translate($name/n1:family, $smallcase, $uppercase)"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="$name/n1:given"/>
				<xsl:if test="$name/n1:suffix">
					<xsl:text> </xsl:text>
					<xsl:value-of select="$name/n1:suffix"/>
				</xsl:if>
				<xsl:if test="$name/n1:prefix">
					<xsl:text> </xsl:text>(<xsl:value-of select="$name/n1:prefix"/>)
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$name"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Format NHS number as per CUI guidance: ### ### #### -->
	<xsl:template name="cuiNHSNo">
		<xsl:param name="nhsNo"/>
		<xsl:value-of select="substring($nhsNo, 1, 3)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring($nhsNo, 3, 3)"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="substring($nhsNo, 7)"/>
	</xsl:template>
	<!--  Format Date 
    outputs a date in Month Day, Year form
    -->
	<xsl:template name="formatDate">
		<xsl:param name="date"/>
		<xsl:variable name="month" select="substring ($date, 5, 2)"/>
		<xsl:value-of select="substring ($date, 7, 2)"/>
		<xsl:if test="substring ($date, 7, 2)">
			<xsl:text>-</xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$month='01'">
				<xsl:text>Jan</xsl:text>
			</xsl:when>
			<xsl:when test="$month='02'">
				<xsl:text>Feb</xsl:text>
			</xsl:when>
			<xsl:when test="$month='03'">
				<xsl:text>Mar</xsl:text>
			</xsl:when>
			<xsl:when test="$month='04'">
				<xsl:text>Apr </xsl:text>
			</xsl:when>
			<xsl:when test="$month='05'">
				<xsl:text>May</xsl:text>
			</xsl:when>
			<xsl:when test="$month='06'">
				<xsl:text>Jun</xsl:text>
			</xsl:when>
			<xsl:when test="$month='07'">
				<xsl:text>Jul</xsl:text>
			</xsl:when>
			<xsl:when test="$month='08'">
				<xsl:text>Aug</xsl:text>
			</xsl:when>
			<xsl:when test="$month='09'">
				<xsl:text>Sep</xsl:text>
			</xsl:when>
			<xsl:when test="$month='10'">
				<xsl:text>Oct</xsl:text>
			</xsl:when>
			<xsl:when test="$month='11'">
				<xsl:text>Nov</xsl:text>
			</xsl:when>
			<xsl:when test="$month='12'">
				<xsl:text>Dec</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring ($date, 1, 4)"/>
		<xsl:if test="string-length($date)>8">
			<xsl:text>, </xsl:text>
			<xsl:value-of select="substring ($date, 9, 2)"/>
			<xsl:text>:</xsl:text>
			<xsl:value-of select="substring ($date, 11, 2)"/>
		</xsl:if>
	</xsl:template>

	<!-- Link Html -->
	<xsl:template match="n1:linkHtml">
		<xsl:element name="a">
			<xsl:for-each select="@*">
				<xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
			</xsl:for-each>
			<xsl:apply-templates/>
			<xsl:value-of select="link_html"/>
		</xsl:element>
	</xsl:template>

	<!--  Header  -->
	<xsl:template name="header">
		<div class="header">
			<table class="header">
				<tr>
					<th class="header">
						<xsl:text>Document Created</xsl:text>
					</th>
					<td class="header">
						<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="n1:effectiveTime/@value"/>
						</xsl:call-template>
					</td>
				</tr>
				<xsl:for-each select="n1:author">
					<tr>
						<th class="header">
							<xsl:text>Authored by</xsl:text>
						</th>
						<td class="header">
							<xsl:choose>
								<xsl:when test="n1:COCD_TP145231GB01.OriginatingSystem/n1:representedOrganization/n1:name">
									<xsl:value-of select="n1:COCD_TP145231GB01.OriginatingSystem/n1:representedOrganization/n1:name"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Unknown</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="n1:informationRecipient">
					<tr>
						<th class="header">
							<xsl:text>Recipient </xsl:text>
						</th>
						<td class="header">
							<xsl:if test="n1:COCD_TP145218GB01.IntendedRecipient/n1:representedOrganization/n1:name">
								<xsl:value-of select="n1:COCD_TP145218GB01.IntendedRecipient/n1:representedOrganization/n1:name"/>
							</xsl:if>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="n1:performer">
					<tr>
						<th class="header">
							<xsl:text>Contact Name </xsl:text>
						</th>
						<td class="header">
							<xsl:if test="n1:COCD_TP145217GB01.ContactPerson/n1:assignedPerson/n1:name">
								<xsl:value-of select="n1:COCD_TP145217GB01.ContactPerson/n1:assignedPerson/n1:name"/>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<th class="header">
							<xsl:text>Contact Organisation </xsl:text>
						</th>
						<td class="header">
							<xsl:if test="n1:COCD_TP145217GB01.ContactPerson/n1:representedOrganization/n1:name">
								<xsl:value-of select="n1:COCD_TP145217GB01.ContactPerson/n1:representedOrganization/n1:name"/>
							</xsl:if>
						</td>
					</tr>
					<tr>
						<th class="header">
							<xsl:text>Contact Address </xsl:text>
						</th>
						<td class="header">
							<xsl:if test="n1:COCD_TP145217GB01.ContactPerson/n1:addr">
								<xsl:value-of select="n1:COCD_TP145217GB01.ContactPerson/n1:addr"/>
							</xsl:if>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>

	<!--  Event Information  -->
	<xsl:template match="n1:component/n1:eventInformation">
		<div class="titlebar">
			<div class="doctitle">
				Patient Event Notification
			</div>
			<table class="titlebar">
				<!-- Event Time -->
				<tr>
					<th class="titlebar">
						<xsl:text>Event Time</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:call-template name="formatDate">
							<xsl:with-param name="date" select="n1:effectiveTime/@value"/>
						</xsl:call-template>
					</td>
				</tr>
				<!-- Event Type -->
				<tr>
					<th class="titlebar">
						<xsl:text>Event Type</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:code/@displayName">
							<xsl:value-of select="n1:code/@displayName"/>
						</xsl:if>
					</td>
				</tr>
				<!-- Event Subtype -->
				<tr>
					<th class="titlebar">
						<xsl:text>Event Sub-Type</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component4/n1:eventSubtype/n1:value/@displayName">
							<xsl:value-of select="n1:component4/n1:eventSubtype/n1:value/@displayName"/>
						</xsl:if>
					</td>
				</tr>
			</table>
			<div class="docsubtitle">
				Associated Document
			</div>
			<table class="titlebar">
				<!-- Document Type -->
				<tr>
					<th class="titlebar">
						<xsl:text>Document Type</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component/n1:documentDetails/n1:code/@displayName">
							<xsl:value-of select="n1:component/n1:documentDetails/n1:code/@displayName"/>
						</xsl:if>
					</td>
				</tr>
				<!-- Document Format -->
				<tr>
					<th class="titlebar">
						<xsl:text>Document Format</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component2/n1:documentFormat/n1:value/@code">
							<xsl:value-of select="n1:component2/n1:documentFormat/n1:value/@code"/>
						</xsl:if>
					</td>
				</tr>
				<!-- Document Profile ID -->
				<tr>
					<th class="titlebar">
						<xsl:text>Document Format</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component3/n1:profileID/n1:value/@extension">
							<xsl:value-of select="n1:component3/n1:profileID/n1:value/@extension"/>
						</xsl:if>
					</td>
				</tr>
				<!-- Document Set ID -->
				<tr>
					<th class="titlebar">
						<xsl:text>Document Set ID</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component/n1:documentDetails/n1:setId/@extension">
							<xsl:value-of select="n1:component/n1:documentDetails/n1:setId/@extension"/>
						</xsl:if>
					</td>
				</tr>
				<!-- Document ID -->
				<tr>
					<th class="titlebar">
						<xsl:text>Document Instance ID</xsl:text>
					</th>
					<td class="titlebar">
						<xsl:if test="n1:component/n1:documentDetails/n1:id/@extension">
							<xsl:value-of select="n1:component/n1:documentDetails/n1:id/@extension"/>
						</xsl:if>
					</td>
				</tr>
			</table>
			<div class="docsubtitle">
				Associated Links
			</div>
			<table class="titlebar">
				<!-- URL -->
				<xsl:for-each select="n1:component1">
					<tr>
						<td class="titlebar">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="n1:uRL/n1:value"/>
								</xsl:attribute>
								<xsl:value-of select="n1:uRL/n1:code/@displayName"/>
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>

	
</xsl:stylesheet>
