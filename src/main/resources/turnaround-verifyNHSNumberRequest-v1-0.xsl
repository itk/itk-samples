<!-- Note this was copied/refactored from itk-cloudharness module on 14/12/2012 by MOS-->
<xsl:stylesheet version="2.0" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:hl7="urn:hl7-org:v3"
        exclude-result-prefixes="xs hl7">
        
    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:param name="response-msg-id"/>
    
    <xsl:template match="/">
		<verifyNHSNumberResponse-v1-0 xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" moodCode="EVN" classCode="OBS">
            <id root="{$response-msg-id}"/>
            <code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.284" code="verifyNHSNumberResponse-v1-0"/>
            <value codeSystem="2.16.840.1.113883.2.1.3.2.4.17.285" code="SMSP-0000"/>
            <component typeCode="COMP">
                <validIdentifier moodCode="EVN" classCode="OBS">
                    <code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.287" code="VI"/>
                    <value value="true"/>
                    <subject typeCode="SBJ">
                        <patient classCode="PAT">
                            <id root="2.16.840.1.113883.2.1.4.1" extension="{//hl7:Person.NHSNumber/hl7:value/@extension}"/>
                        </patient>
                    </subject>
                </validIdentifier>
            </component>
        </verifyNHSNumberResponse-v1-0>
    </xsl:template>
    
</xsl:stylesheet>    