<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xs fn">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:template match="/">

		<getPatientDetailsByNHSNumberRequest-v1-0 xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" moodCode="EVN" classCode="CACT">
			<id>
				<xsl:attribute name="root">
					<xsl:value-of select="Message/MessageId"/>
				</xsl:attribute>
			</id>
			<code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.284" code="getPatientDetailsByNHSNumberRequest-v1-0"/>
			<queryEvent>
				<Person.DateOfBirth>
					<value>
						<xsl:attribute name="value">
							<xsl:value-of select="Message/DateOfBirth"/>
						</xsl:attribute>
					</value>
					<semanticsText>Person.DateOfBirth</semanticsText>
				</Person.DateOfBirth>
				<Person.NHSNumber>
					<value root="2.16.840.1.113883.2.1.4.1">
						<xsl:attribute name="extension">
							<xsl:value-of select="Message/NHSNumber"/>
						</xsl:attribute>
					</value>
					<semanticsText>Person.NHSNumber</semanticsText>
				</Person.NHSNumber>
			</queryEvent>
		</getPatientDetailsByNHSNumberRequest-v1-0>
		
	</xsl:template>
</xsl:stylesheet>
