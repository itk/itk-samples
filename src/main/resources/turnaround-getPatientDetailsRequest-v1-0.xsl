<!-- Note this was copied/refactored from itk-cloudharness module on 14/12/2012 by MOS-->
<xsl:stylesheet version="2.0" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:hl7="urn:hl7-org:v3"
        exclude-result-prefixes="xs hl7">
        
    <xsl:output method="xml"/>
    
    <xsl:param name="response-msg-id"/>
    
    <xsl:template match="/">
		<getPatientDetailsResponse-v1-0 xmlns="urn:hl7-org:v3" moodCode="EVN" classCode="OBS">
			<id root="{$response-msg-id}" />
			<code codeSystem="2.16.840.1.113883.2.1.3.2.4.17.284" code="getPatientDetailsResponse-v1-0" />
			<value codeSystem="2.16.840.1.113883.2.1.3.2.4.17.285" code="SMSP-0000" />
			<subject typeCode="SBJ">
				<patient classCode="PAT">
					<!--
						Just hard code the NHS Number as this is used for all get patient details requests which may not all have NHS No  
						<id root="2.16.840.1.113883.2.1.4.1" extension="{//hl7:Person.NHSNumber/hl7:value/@extension}" />
					 -->
					 <id root="2.16.840.1.113883.2.1.4.1" extension="1234512345" />
					<name>
						<given>John</given>
						<family>Smith</family>
					</name>
					<addr use="H">
						<postalCode>LS17 7TR</postalCode>
						<streetAddressLine>41 Manor Grove</streetAddressLine>
						<streetAddressLine>Little Manorton</streetAddressLine>
						<streetAddressLine>Leeds</streetAddressLine>
						<streetAddressLine />
						<streetAddressLine />
					</addr>
					<patientPerson determinerCode="INSTANCE" classCode="PSN">
						<administrativeGenderCode code="1" codeSystem="2.16.840.1.113883.2.1.3.2.4.16.25" />
						<birthTime value="{//hl7:Person.DateOfBirth/hl7:value/@value}" />
						<gPPractice classCode="SDLOC">
							<addr>
								<postalCode>LS17 8TY</postalCode>
								<streetAddressLine>2 High Street</streetAddressLine>
								<streetAddressLine>Little Manorton</streetAddressLine>
								<streetAddressLine>Leeds</streetAddressLine>
								<streetAddressLine />
								<streetAddressLine />
							</addr>
							<locationOrganization determinerCode="INSTANCE" classCode="ORG">
								<id root="2.16.840.1.113883.2.1.3.2.4.19.2" extension="YT4" />
								<name>Grove GP Surgery</name>
							</locationOrganization>
						</gPPractice>
					</patientPerson>
				</patient>
			</subject>
		</getPatientDetailsResponse-v1-0>
    </xsl:template>
    
</xsl:stylesheet>    