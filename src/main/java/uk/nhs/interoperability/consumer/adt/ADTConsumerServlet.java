/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer.adt;

import uk.nhs.interoperability.consumer.AbstractSimpleMessageServlet;
import uk.nhs.interoperability.consumer.ITKMessageConsumer;
import uk.nhs.interoperability.consumer.appemulator.SimpleApplicationEmulator;

/**
 * The Class ADTConsumerServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ADTConsumerServlet extends AbstractSimpleMessageServlet {
	
	private static final long serialVersionUID = 3418513641832733244L;
	
	private ITKMessageConsumer applicationLayer;

	/**
	 * Instantiates a new aDT consumer servlet.
	 */
	public ADTConsumerServlet() {
		/*
		 * In reality this would be bound to a real application
		 * layer component - using a simple emulator for the
		 * samples
		 */		
		this.applicationLayer = new SimpleApplicationEmulator();
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.AbstractSimpleMessageServlet#getMessageConsumer()
	 */
	@Override
	public ITKMessageConsumer getMessageConsumer() {
		return this.applicationLayer;
	}

}
