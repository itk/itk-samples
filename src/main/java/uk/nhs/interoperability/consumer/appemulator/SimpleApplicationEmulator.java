/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.consumer.appemulator;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import uk.nhs.interoperability.consumer.ITKMessageConsumer;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.ITKSimpleMessageResponse;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.HL7Utils;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class SimpleApplicationEmulator.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class SimpleApplicationEmulator implements ITKMessageConsumer, Runnable {

	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:TESTORGS:ORGA";

	/** The executor service. */
	private ExecutorService executorService;
	
	/** The is running. */
	private boolean isRunning = true;
	
	/** The itk message sender. */
	private ITKMessageSender itkMessageSender;
	
	/** The props. */
	private Properties props = new Properties();
	
	/** The audit identity. */
	private String auditIdentity;
	
	/*
	 * Construct an in-memory queue to queue ITK messages request for
	 * subsequent asynchronous processing - this emulates a common
	 * pattern in many application where inbound messages are queued
	 * for processing 
	 */
	/** The async processing queue. */
	private Queue<ITKMessage> asyncProcessingQueue = new LinkedBlockingQueue<ITKMessage>();
	
	/**
	 * Instantiates a new simple application emulator.
	 */
	public SimpleApplicationEmulator() {
		this.executorService = Executors.newFixedThreadPool(1);
		this.executorService.execute(this);
		this.itkMessageSender = new ITKMessageSenderImpl();
		this.props = new Properties();
		try {
			props.load(this.getClass().getResourceAsStream("/consumeremulator.properties"));
		} catch (IOException e) {
			Logger.error("Could not load consumer emulator properties - emulator not likely to behave correctly", e);
		}
		this.auditIdentity = this.props.getProperty("audit.identity");
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.ITKMessageConsumer#onSyncMessage(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public ITKMessage onSyncMessage(ITKMessage request) throws ITKMessagingException {
		Logger.debug("Application invoked");
		ITKMessage response = this.processMessage(request);
		response.getMessageProperties().setFromAddress(new ITKAddressImpl(FROMADDRESS));

		return response;
	}
	
	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.ITKMessageConsumer#onMessage(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void onMessage(ITKMessage request) throws ITKMessagingException {
		//Do some (really) basic validation of the message
		if (request == null) {
			throw new ITKMessagingException("The request was null - could not process");
		}
		if (request.getBusinessPayload() == null && request.getMessageProperties() == null) {
			throw new ITKMessagingException(request.getMessageProperties(), ITKMessagingException.INVALID_MESSAGE_CODE, "The request message properties or contents were null - message cannot be processed");
		}
		//Queue for asynchronous processing
		this.asyncProcessingQueue.add(request);
	}
	
	/**
	 * Process async message.
	 *
	 * @param request the request
	 */
	private void processAsyncMessage(ITKMessage request) {
		Logger.debug("Processing queued itk request");
		try {
			ITKMessage response = this.processMessage(request);
			if (response != null) {
				response.getMessageProperties().setFromAddress(new ITKAddressImpl(FROMADDRESS));
				this.itkMessageSender.send(response);
			} else {
				Logger.info("No response configured/created for " + request);
			}
		} catch (ITKMessagingException e) {
			/*
			 * In a real application some more in-depth error
			 * handling may occur such as storing the failed message
			 * for attention of an administrator, however for this
			 * simple application emulator we just log an error
			 */
			Logger.error("Could not send aysnchronous response", e);
		}
	}
	
	/**
	 * Process message.
	 *
	 * @param request the request
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage processMessage(ITKMessage request) throws ITKMessagingException {
		
		String requestMsgService = request.getMessageProperties().getServiceId();	
		/*
		 * This simple emulator application's response behaviour is determined
		 * via configuration - more sophisticated measures are likely to exist in
		 * a real world implementation
		 */
		String responseType = this.props.getProperty(requestMsgService + ".response.type");
		
		//Conditional logic depending on type of response
		if (responseType == null) {
			//Ooops no response configured
			throw new ITKMessagingException("Incorrect emulator configuration - no response type configured for " + requestMsgService);
			
		} else if (responseType.equalsIgnoreCase("simple")) {
			//Simple message response - can be generated directly
			Logger.trace("Creating a simple message response");
			return new ITKSimpleMessageResponse(request.getMessageProperties(), true);
			
		} else if (responseType.equalsIgnoreCase("fullResponse")) {
			//Full business response - construct via XSLT
			String responseProfileId = this.props.getProperty(request.getMessageProperties().getServiceId() + "Response.profileId");
			Logger.trace("Creating a business response");
			return this.turnaroundViaXSLT(responseProfileId, request);
			
		} else if (responseType.equalsIgnoreCase("fixedResponse")) {
			// Fixed response - construct via fixed response for message type
			String responseProfileId = this.props.getProperty(request.getMessageProperties().getServiceId() + "Response.profileId");
			Logger.trace("Creating a Fixed response");
			return this.turnaroundViaFile(responseProfileId, request);

		} else if (responseType.equalsIgnoreCase("businessAck")) {
			//Create a business ack if required
			return this.createBusinessAck(request);
		}  else if (responseType.equalsIgnoreCase("none")) {
			//No business response is expected
			return null;
		} else {
			//Whilst something was configured the value was unexpected
			throw new ITKMessagingException("Incorrect emulator configuration - unknown response type (" + responseType + ") configured for " + requestMsgService);
		}
	}
	
	/**
	 * Creates the business ack.
	 *
	 * @param request the request
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage createBusinessAck(ITKMessage request) throws ITKMessagingException {
		//Create a businessAck if requested
		String businessAckHandlingSpec = request.getMessageProperties().getHandlingSpecification(ITKMessageProperties.BUSINESS_ACK_HANDLING_SPECIFICATION_KEY);
		if (businessAckHandlingSpec != null && businessAckHandlingSpec.equals("true")) {
			String businessAckService = "urn:nhs-itk:services:201005:SendBusinessAck-v1-0";
			String responseProfileId = this.props.getProperty(businessAckService + ".profileId");
			Logger.trace("Creating a business response");
			ITKMessage msg = this.turnaroundViaXSLT(responseProfileId, request);
			msg.getMessageProperties().setServiceId(businessAckService);
			return msg;
		} else {
			Logger.trace("No handling specification for business ack - not creating a business Ack");
		}
		//Otherwise no response is required
		return null;
	}
	
	/**
	 * Turnaround via xslt.
	 *
	 * @param responseProfileId the response profile id
	 * @param request the request
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage turnaroundViaXSLT(String responseProfileId, ITKMessage request) throws ITKMessagingException {
		//Use simple XSLTs to create appropriate responses
		String xslt = this.props.getProperty(request.getMessageProperties().getServiceId() + ".turnaround.xslt");
		if (xslt != null) {
			
			//Create a response using the message properties from the request
			SimpleMessage msg = new SimpleMessage(request.getMessageProperties(), this.auditIdentity, responseProfileId, true);
			UUID messageId = UUID.randomUUID();
			msg.getMessageProperties().setBusinessPayloadId(messageId.toString().toUpperCase());
			
			Logger.trace("Using " + xslt + " to turnaround request");
			String inputXML = request.getBusinessPayload();
			//Logger.trace("XSLT input " + inputXML);
			String outputXML = TransformManager.doTransform(xslt, inputXML, this.getTransformParameters(msg));
			//Logger.trace("XSLT output " + outputXML);			
			msg.setBusinessPayload(outputXML);
			
			return msg;			
		} else {
			Logger.warn("Could not use XSLT to turnaround request");
		}
		return null;
	}
	
	/**
	 * Turnaround via file.
	 *
	 * @param responseProfileId the response profile id
	 * @param request the request
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	private ITKMessage turnaroundViaFile(String responseProfileId, ITKMessage request) throws ITKMessagingException {
		
		//Use fixed file response
		String fileName = this.props.getProperty(request.getMessageProperties().getServiceId() + ".turnaround.txt");
		if (fileName != null) {
			
			
			//Create a response using the message properties from the request
			SimpleMessage msg = new SimpleMessage(request.getMessageProperties(), this.auditIdentity, responseProfileId, true);
			UUID messageId = UUID.randomUUID();
			msg.getMessageProperties().setBusinessPayloadId(messageId.toString().toUpperCase());
			
			Logger.trace("Using " + fileName + " to turnaround request");
			String responseString;
			try {
				responseString = readFile(fileName);
			} catch (IOException e) {
				e.printStackTrace();
				throw new ITKMessagingException("Incorrect emulator configuration - error reading response file (" + fileName);
			}
			msg.setBusinessPayload(responseString);
			return msg;			

		} else {
			Logger.warn("Could not load turnaround request from file.");
		}
		return null;
	}
	
	/**
	 * Extract some common properties from the newly created
	 * message to pass to the XSLT transform as a map of parameters.
	 *
	 * @param itkMessage The skeleton ItkMessage whose contents will be created
	 * as a result of the transform
	 * @return A Map containing the appropriate XSLT transform parameters including items
	 * such as the new message Id, sender, receiver etc.
	 */
	private Map<String, String> getTransformParameters(ITKMessage itkMessage) {
		if (itkMessage != null && itkMessage.getMessageProperties() != null) {
			 ITKMessageProperties msgProps = itkMessage.getMessageProperties();
			 Map<String, String> params = new HashMap<String, String>();
			 params.put("response-msg-id", msgProps.getBusinessPayloadId());
			 params.put("from-address", msgProps.getFromAddress().getURI());
			 params.put("to-address", msgProps.getToAddress().getURI());
			 params.put("creation-time", HL7Utils.getHL7DateTime());
			 return params;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (this.isRunning) {
			try {
				if (this.asyncProcessingQueue.isEmpty()) {
					Thread.sleep(5000);
				} else {
					this.processAsyncMessage(this.asyncProcessingQueue.poll());
				}
			} catch (InterruptedException e) {
				this.isRunning = false;
			}
		}
	}
	
	/**
	 * Read file.
	 *
	 * @param fname the fname
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String readFile(String fname) throws IOException {

	    InputStream tis = this.getClass().getResourceAsStream("/messages/"+fname);
	    StringBuilder fileContents = new StringBuilder();
	    Scanner scanner = new Scanner(tis);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}

}
