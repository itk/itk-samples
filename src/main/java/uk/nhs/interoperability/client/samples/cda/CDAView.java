/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.cda;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class CDAView.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class CDAView extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new cDA view.
     *
     * @see HttpServlet#HttpServlet()
     */
    public CDAView() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String docId    = request.getParameter("docid");
		Logger.trace("Document ID:"+docId);
		HttpSession session = request.getSession(true);
        session.removeAttribute("errorMessage");
        session.removeAttribute("outcomeMessage");
        session.removeAttribute("responseMessage");

		String document = DocStore.getDocument(docId);

		if (null!=document){
			String htmlResponse = "";
			try {
				htmlResponse = TransformManager.doTransform("CDA_NPfIT_Document_Renderer.xsl", document);
				session.setAttribute("outcomeMessage", "CDA Message Sent.");
				session.setAttribute("responseMessage", htmlResponse);
			} catch (Exception e) {
				Logger.error("Document Transform failed:",e);
				session.setAttribute("errorMessage", "Document Transform Failed.");
			}
			Logger.trace(htmlResponse);
		} else {
			Logger.trace("Document not found");
			session.setAttribute("errorMessage", "Document Not Found.");
		}
		response.sendRedirect("./CDAView.jsp");
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("this is doPost");
		String docId= request.getParameter("docid");
		response.getWriter().write("DO POST. DOCID:"+docId);
	}

}
