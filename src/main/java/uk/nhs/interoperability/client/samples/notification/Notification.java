/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import java.util.UUID;

import uk.nhs.interoperability.transform.TransformManager;

/**
 * The Class Notification.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class Notification {

	/** The message id. */
	String messageId;
	
	/** The nhs number. */
	String nhsNumber;
	
	/** The date of birth. */
	String dateOfBirth;
	
	/** The sender org. */
	String senderOrg;
	
	/** The recipient org. */
	String recipientOrg;
	
	/** The recipient address. */
	String recipientAddress;
	
	/** The patient name. */
	String patientName;
	
	/** The patient address. */
	String patientAddress;
	
	/** The event type code. */
	String eventTypeCode;
	
	/** The event type desc. */
	String eventTypeDesc;
	
	/** The contact name. */
	String contactName;
	
	/** The effective time. */
	String effectiveTime;
	
	/** The contact address. */
	String contactAddress;
	
	/** The contact org. */
	String contactOrg;
	
	/**
	 * Gets the sender org.
	 *
	 * @return the sender org
	 */
	public String getSenderOrg() {
		return senderOrg;
	}
	
	/**
	 * Sets the sender org.
	 *
	 * @param senderOrg the new sender org
	 */
	public void setSenderOrg(String senderOrg) {
		this.senderOrg = senderOrg;
	}
	
	/**
	 * Gets the recipient org.
	 *
	 * @return the recipient org
	 */
	public String getRecipientOrg() {
		return recipientOrg;
	}
	
	/**
	 * Sets the recipient org.
	 *
	 * @param recipientOrg the new recipient org
	 */
	public void setRecipientOrg(String recipientOrg) {
		this.recipientOrg = recipientOrg;
	}
	
	/**
	 * Gets the recipient address.
	 *
	 * @return the recipient address
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}
	
	/**
	 * Sets the recipient address.
	 *
	 * @param recipientAddress the new recipient address
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	
	/**
	 * Gets the patient name.
	 *
	 * @return the patient name
	 */
	public String getPatientName() {
		return patientName;
	}
	
	/**
	 * Sets the patient name.
	 *
	 * @param patientName the new patient name
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 * Gets the patient address.
	 *
	 * @return the patient address
	 */
	public String getPatientAddress() {
		return patientAddress;
	}
	
	/**
	 * Sets the patient address.
	 *
	 * @param patientAddress the new patient address
	 */
	public void setPatientAddress(String patientAddress) {
		this.patientAddress = patientAddress;
	}
	
	/**
	 * Gets the event type code.
	 *
	 * @return the event type code
	 */
	public String getEventTypeCode() {
		return eventTypeCode;
	}
	
	/**
	 * Sets the event type code.
	 *
	 * @param eventTypeCode the new event type code
	 */
	public void setEventTypeCode(String eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	
	/**
	 * Gets the event type desc.
	 *
	 * @return the event type desc
	 */
	public String getEventTypeDesc() {
		return eventTypeDesc;
	}
	
	/**
	 * Sets the event type desc.
	 *
	 * @param eventTypeDesc the new event type desc
	 */
	public void setEventTypeDesc(String eventTypeDesc) {
		this.eventTypeDesc = eventTypeDesc;
	}
	
	/**
	 * Gets the contact name.
	 *
	 * @return the contact name
	 */
	public String getContactName() {
		return contactName;
	}
	
	/**
	 * Sets the contact name.
	 *
	 * @param contactName the new contact name
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	/**
	 * Gets the effective time.
	 *
	 * @return the effective time
	 */
	public String getEffectiveTime() {
		return effectiveTime;
	}
	
	/**
	 * Sets the effective time.
	 *
	 * @param effectiveTime the new effective time
	 */
	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}
	
	/**
	 * Gets the contact address.
	 *
	 * @return the contact address
	 */
	public String getContactAddress() {
		return contactAddress;
	}
	
	/**
	 * Sets the contact address.
	 *
	 * @param contactAddress the new contact address
	 */
	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}
	
	/**
	 * Gets the contact org.
	 *
	 * @return the contact org
	 */
	public String getContactOrg() {
		return contactOrg;
	}
	
	/**
	 * Sets the contact org.
	 *
	 * @param contactOrg the new contact org
	 */
	public void setContactOrg(String contactOrg) {
		this.contactOrg = contactOrg;
	}
	
	/**
	 * Sets the message id.
	 *
	 * @param messageId the new message id
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return messageId;
	}
	
	/**
	 * Gets the nHS number.
	 *
	 * @return the nHS number
	 */
	public String getNHSNumber() {
		return nhsNumber;
	}
	
	/**
	 * Sets the nHS number.
	 *
	 * @param nhsNumber the new nHS number
	 */
	public void setNHSNumber(String nhsNumber) {
		this.nhsNumber = nhsNumber;
	}
	
	/**
	 * Gets the date of birth.
	 *
	 * @return the date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	/**
	 * Sets the date of birth.
	 *
	 * @param dateOfBirth the new date of birth
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * Serialise.
	 *
	 * @return the string
	 */
	public String serialise(){
		String XML = "<Message>";
		XML += "<MessageId>"+messageId+"</MessageId>";
		
		XML += "<EffectiveTime>"+effectiveTime+"</EffectiveTime>";
		XML += "<DateOfBirth>"+dateOfBirth+"</DateOfBirth>";
		XML += "<SenderOrg>"+senderOrg+"</SenderOrg>";
		XML += "<RecipientOrg>"+recipientOrg+"</RecipientOrg>";
		XML += "<RecipientAddress>"+recipientAddress+"</RecipientAddress>";
		XML += "<PatientName>"+patientName+"</PatientName>";
		XML += "<PatientAddress>"+patientAddress+"</PatientAddress>";
		XML += "<NHSNumber>"+nhsNumber+"</NHSNumber>";
		XML += "<EventTypeCode>"+eventTypeCode+"</EventTypeCode>";
		XML += "<EventTypeDesc>"+eventTypeDesc+"</EventTypeDesc>";
		XML += "<ContactName>"+contactName+"</ContactName>";
		XML += "<ContactAddress>"+contactAddress+"</ContactAddress>";
		XML += "<ContactOrg>"+contactOrg+"</ContactOrg>";		
		XML += "</Message>";
		String serialisedMessage ="";
		try {
			serialisedMessage = TransformManager.doTransform("ToNotification.xsl", XML);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serialisedMessage;
	}
	
	/**
	 * Instantiates a new notification.
	 */
	public Notification(){
		UUID messageId = UUID.randomUUID();
		this.messageId = messageId.toString().toUpperCase();
	}

}
