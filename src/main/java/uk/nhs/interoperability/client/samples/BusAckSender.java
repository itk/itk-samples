/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.UUID;

import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class BusAckSender.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class BusAckSender {

	/** The Constant CALLBACK. */
	private static final String CALLBACK = "urn:nhs-uk:addressing:ods:Y88764:CALLBACK";

	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:R59:oncology";

	/** The Constant BUS_ACK_SERVICE. */
	private static final String BUS_ACK_SERVICE = "urn:nhs-itk:services:201005:SendBusinessAck-v1-0";
	
	/** The Constant BUS_ACK_PROFILEID. */
	private static final String BUS_ACK_PROFILEID = "urn:nhs-itk:profile:201005:SendBusinessAck-v1-0";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		BusAckSender appA = new BusAckSender();
		appA.sendAsyncMessage();
	
	}

	/**
	 * Send async message.
	 */
	private void sendAsyncMessage(){
		Logger.trace("*** BusAckSender: Starting sendSyncMessage");
		
		// Create the message
		ITKMessage msg = new SimpleMessage();

		// populate the payload
		try {
			msg.setBusinessPayload(readFile("BusinessAck.xml"));
		} catch (IOException e1) {
			Logger.error("Error Loading TEST BUS ACK Message",e1);
			return;
		}
		
		UUID messageUUID = UUID.randomUUID();
		String messageId = messageUUID.toString();
		
		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(new ITKAddressImpl(CALLBACK));
		mp.setServiceId(BUS_ACK_SERVICE);
		mp.setBusinessPayloadId(messageId);
		mp.setProfileId(BUS_ACK_PROFILEID);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			// Send this message synchronously. The response message will be returned
			sender.sendAsync(msg);
		
			Logger.trace("BusAckSender sent message:");
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.trace("*** BusAckSender: Ending sendAsyncMessage");
	}

	/**
	 * Read file.
	 *
	 * @param fname the fname
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String readFile(String fname) throws IOException {

	    InputStream tis = this.getClass().getResourceAsStream("/messages/"+fname);
	    StringBuilder fileContents = new StringBuilder();
	    Scanner scanner = new Scanner(tis);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}

}
