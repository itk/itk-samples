/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.smsp;

import java.util.UUID;

import uk.nhs.interoperability.transform.TransformManager;

/**
 * The Class VerifyNHSNumberRequest.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class VerifyNHSNumberRequest {

	/** The message id. */
	String messageId;
	
	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return messageId;
	}
	
	/**
	 * Gets the nHS number.
	 *
	 * @return the nHS number
	 */
	public String getNHSNumber() {
		return nhsNumber;
	}
	
	/**
	 * Sets the nHS number.
	 *
	 * @param nhsNumber the new nHS number
	 */
	public void setNHSNumber(String nhsNumber) {
		this.nhsNumber = nhsNumber;
	}
	
	/**
	 * Gets the given name.
	 *
	 * @return the given name
	 */
	public String getGivenName() {
		return givenName;
	}
	
	/**
	 * Sets the given name.
	 *
	 * @param givenName the new given name
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	
	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/**
	 * Gets the date of birth.
	 *
	 * @return the date of birth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	/**
	 * Sets the date of birth.
	 *
	 * @param dateOfBirth the new date of birth
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * Gets the postcode.
	 *
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}
	
	/**
	 * Sets the postcode.
	 *
	 * @param postcode the new postcode
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	/** The nhs number. */
	String nhsNumber;
	
	/** The given name. */
	String givenName;
	
	/** The surname. */
	String surname;
	
	/** The date of birth. */
	String dateOfBirth;
	
	/** The postcode. */
	String postcode;
	
	/**
	 * Serialise.
	 *
	 * @return the string
	 */
	public String serialise(){
		String XML = "<Message>";
		XML += "<MessageId>"+messageId+"</MessageId>";
		XML += "<NHSNumber>"+nhsNumber+"</NHSNumber>";
		XML += "<GivenName>"+givenName+"</GivenName>";
		XML += "<Surname>"+surname+"</Surname>";
		XML += "<DateOfBirth>"+dateOfBirth+"</DateOfBirth>";
		XML += "<Postcode>"+postcode+"</Postcode>";
		XML += "</Message>";
		String serialisedMessage ="";
		try {
			serialisedMessage = TransformManager.doTransform("ToVerifyNHSNumberRequest.xslt", XML);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serialisedMessage;
	}
	
	/**
	 * Instantiates a new verify nhs number request.
	 */
	public VerifyNHSNumberRequest(){
		UUID messageId = UUID.randomUUID();
		this.messageId = messageId.toString().toUpperCase();
	}

}
