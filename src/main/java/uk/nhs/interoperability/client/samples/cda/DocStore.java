/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.cda;

import java.util.HashMap;

/**
 * The Class DocStore.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class DocStore {

	/** The messages. */
	private static HashMap<String, String> messages = new HashMap<String, String>();

	/**
	 * Put document.
	 *
	 * @param id the id
	 * @param document the document
	 */
	public static void putDocument(String id, String document){
		messages.put(id,document);
		
	}
	
	/**
	 * Gets the document.
	 *
	 * @param id the id
	 * @return the document
	 */
	public static String getDocument(String id){
		return messages.get(id);
	}

}
