/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.smsp;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class SpineMiniServicesClient.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class SpineMiniServicesClient {

	/** The Constant ORGB. */
	private static final String ORGB = "urn:nhs-uk:addressing:ods:TESTORGS:ORGB";
	
	/** The Constant ORGA. */
	private static final String ORGA = "urn:nhs-uk:addressing:ods:TESTORGS:ORGA";
	
	/** The Constant CLOUDHARNESS. */
	private static final String CLOUDHARNESS = "urn:nhs-uk:addressing:ods:TESTORGS:CLOUDHARNESS";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGA);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = ORGA;

	/** The Constant VERIFY_NHS. */
	private static final String VERIFY_NHS = "urn:nhs-itk:services:201005:verifyNHSNumber-v1-0";
	
	/** The Constant GET_NHS. */
	private static final String GET_NHS = "urn:nhs-itk:services:201005:getNHSNumber-v1-0";

	/** The Constant VN_PROFILEID. */
	private static final String GNN_PROFILEID = "urn:nhs-en:profile:getNHSNumberRequest-v1-0";

	/** The Constant VN_PROFILEID. */
	private static final String VN_PROFILEID = "urn:nhs-en:profile:verifyNHSNumberRequest-v1-0";

	/** The Constant GET_PD_NHS. */
	private static final String GET_PD_NHS = "urn:nhs-itk:services:201005:getPatientDetailsByNHSNumber-v1-0";
	
	/** The Constant GPDN_PROFILEID. */
	private static final String GPDN_PROFILEID = "urn:nhs-en:profile:getPatientDetailsByNHSNumberRequest-v1-0";
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		if ( args.length < 3){
			Logger.warn("Usage : SpineMiniServicesClient verify|get NHSNumber DOB [destination (ORGB | CLOUDHARNESS)]");
			System.exit(1);
		}
		String service = args[0].toUpperCase();
		if (!( (service.equals("VERIFY")) || 
				(service.equals("GET")) ) ){
			Logger.warn("Usage : SpineMiniServicesClient verify|get NHSNumber DOB");
			System.exit(1);
		}
		
		ITKAddress toAddress = new ITKAddressImpl(ORGB);

		String nhsNumber = args[1];
		String DOB = args[2];
		SpineMiniServicesClient client = new SpineMiniServicesClient();
		
		//Work out whether to send to a local server or cloudharness
		if (args.length == 4) {
			if ("CLOUDHARNESS".equalsIgnoreCase(args[3])) {
				toAddress = new ITKAddressImpl(CLOUDHARNESS);
			}
		}
		
		if (service.equals("GET")){
			GetPatientDetailsByNHSNumberRequest req1 = new GetPatientDetailsByNHSNumberRequest();
			req1.setNHSNumber(nhsNumber);
			req1.setDateOfBirth(DOB);
			GetPatientDetailsByNHSNumberResponse resp1 = client.getPatientDetailsByNHSNumber(req1, toAddress);
			if (resp1 != null) {
				Logger.info("SMSP GPDBNN COMPLETE. RESPONSE CODE:" + resp1.getResponseCode());
			}
		}

		if (service.equals("VERIFY")){
			VerifyNHSNumberRequest req2 = new VerifyNHSNumberRequest();
			req2.setNHSNumber(nhsNumber);
			req2.setDateOfBirth(DOB);
			VerifyNHSNumberResponse resp2 = client.verifyNHSNumber(req2, toAddress);
			if (resp2 != null) {
				Logger.info("SMSP VNN COMPLETE. RESPONSE CODE:" + resp2.getResponseCode());
			}
		}
	
	}

	/**
	 * Verify nhs number.
	 *
	 * @param request the request
	 * @param toAddress the to address
	 * @return the verify nhs number response
	 */
	public VerifyNHSNumberResponse verifyNHSNumber(VerifyNHSNumberRequest request, 
													ITKAddress toAddress){

		Logger.info("*** SpineMiniServicesClient: Starting verifyNHSNumber");
		
		VerifyNHSNumberResponse response = null;
		// Create the message
		ITKMessage msg = new SimpleMessage();
		msg.setBusinessPayload(request.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(toAddress);
		mp.setServiceId(VERIFY_NHS);
		mp.setBusinessPayloadId(request.getMessageId());
		mp.setProfileId(VN_PROFILEID);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		ITKMessageSender sender = new ITKMessageSenderImpl();
		try {
			ITKMessage resp = sender.sendSync(msg);
			response = new VerifyNHSNumberResponse(resp);
			Logger.trace("SendingApplicationA received response from verifyNHSNumber call:"+resp.getBusinessPayload());
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.info("*** SpineMiniServicesClient: Ending verifyNHSNumber");
		
		return response;
	}
	
	/**
	 * Get NHS number. A Trace Request
	 *
	 * @param request the request
	 * @param toAddress the to address
	 * @return the verify nhs number response
	 */
	public GetNHSNumberResponse getNHSNumber(GetNHSNumberRequest request, 
													ITKAddress toAddress){

		Logger.info("*** SpineMiniServicesClient: Starting getNHSNumber");
		
		GetNHSNumberResponse response = null;
		// Create the message
		ITKMessage msg = new SimpleMessage();
		msg.setBusinessPayload(request.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(toAddress);
		mp.setServiceId(GET_NHS);
		mp.setBusinessPayloadId(request.getMessageId());
		mp.setProfileId(GNN_PROFILEID);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		ITKMessageSender sender = new ITKMessageSenderImpl();
		try {
			ITKMessage resp = sender.sendSync(msg);
			response = new GetNHSNumberResponse(resp);
			Logger.trace("SendingApplicationA received response from getNHSNumber call:"+resp.getBusinessPayload());
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.info("*** SpineMiniServicesClient: Ending getNHSNumber");
		
		return response;
	}

	/**
	 * Gets the patient details by nhs number.
	 *
	 * @param request the request
	 * @param toAddress the to address
	 * @return the patient details by nhs number
	 */
	public GetPatientDetailsByNHSNumberResponse getPatientDetailsByNHSNumber(
													GetPatientDetailsByNHSNumberRequest request, 
													ITKAddress toAddress){

		Logger.info("*** SpineMiniServicesClient: Starting getPatientDetailsByNHSNumber");
		
		GetPatientDetailsByNHSNumberResponse response = null;
		
		ITKMessage msg = new SimpleMessage();
		msg.setBusinessPayload(request.serialise());
		
		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(toAddress);
		mp.setServiceId(GET_PD_NHS);
		mp.setBusinessPayloadId(request.getMessageId());
		mp.setProfileId(GPDN_PROFILEID);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();
		try {
			ITKMessage resp = sender.sendSync(msg);
			response = new GetPatientDetailsByNHSNumberResponse(resp);
			Logger.trace("SendingApplicationA received response from getPatientDetailsByNHSNumber call:"+resp.getBusinessPayload());
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.info("*** SpineMiniServicesClient: Ending getPatientDetailsByNHSNumber");
		
		return response;
	}

}
