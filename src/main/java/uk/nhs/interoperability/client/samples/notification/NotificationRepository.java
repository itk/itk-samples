/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import uk.nhs.interoperability.consumer.AbstractRoutedMessageServlet;
import uk.nhs.interoperability.consumer.ITKMessageConsumer;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class NotificationRepository.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class NotificationRepository extends AbstractRoutedMessageServlet implements ITKMessageConsumer {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -541728545823668074L;

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.ITKMessageConsumer#onMessage(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void onMessage(ITKMessage request) {
		Logger.trace("This is Notification Repository receiving a message:onMessage()");
		String docId = request.getMessageProperties().getBusinessPayloadId();
		if (docId.substring(0,5).equals("uuid_")){
			docId = docId.substring(5);
		}
		NotificationStore.putNotification(docId, request.getBusinessPayload());
		Logger.trace("added notification "+docId+" to repository.");
		
		// NOTE : BUSINESS ACKS ARE NOT SUPPORTED BY THIS VERSION
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.AbstractRoutedMessageServlet#getMessageConsumer()
	 */
	@Override
	public ITKMessageConsumer getMessageConsumer() {
		return this;
	}

	/**
	 * Routed messages do not support true synchronous responses.
	 *
	 * @param request the request
	 * @return the iTK message
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	@Override
	public ITKMessage onSyncMessage(ITKMessage request)	throws ITKMessagingException {
		throw new ITKMessagingException(ITKMessagingException.INVALID_MESSAGE_CODE, "Synchronous execution not supported");
	}

}
