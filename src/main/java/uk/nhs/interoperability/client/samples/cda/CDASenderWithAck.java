/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.cda;

import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class CDASenderWithAck.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class CDASenderWithAck {

	/** The Constant APPB. */
	private static final String APPB = "urn:nhs-uk:addressing:ods:TESTORGS:APPB";

	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:R59:oncology";

	/** The Constant NON_CODED_CDA. */
	private static final String NON_CODED_CDA = "urn:nhs-itk:services:201005:SendCDADocument-v2-0";
	
	/** The Constant NCDA_PROFILEID. */
	private static final String NCDA_PROFILEID = "urn:nhs-en:profile:nonCodedCDADocument-v2-0";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		CDASenderWithAck appA = new CDASenderWithAck();
		appA.sendCDAMessage();
	
	}

	/**
	 * Send cda message.
	 */
	private void sendCDAMessage(){
		Logger.trace("*** CDASender: Starting sendCDAMessage");
		
		// Create the message
		ITKMessage msg = new SimpleMessage();
		NonCodedCDA req = new NonCodedCDA();
		req.setNHSNumber("1234556789");
		req.setDateOfBirth("20020831");
		req.setPresentationText("HELLO WORLD doc 5");
		msg.setBusinessPayload(req.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(new ITKAddressImpl(APPB));
		mp.setServiceId(NON_CODED_CDA);
		//mp.setBusinessPayloadId(req.getMessageId());
		mp.setBusinessPayloadId("HW5");
		mp.setProfileId(NCDA_PROFILEID);

		mp.addHandlingSpecification(ITKMessageProperties.BUSINESS_ACK_HANDLING_SPECIFICATION_KEY, "true");
		mp.addHandlingSpecification(ITKMessageProperties.INTERACTION_HANDLING_SPECIFICATION_KEY, 
				"urn:nhs-itk:interaction:primaryRecipientNonCodedCDADocument-v2-0");
		
		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			// Send this message asynchronously. The response message will be returned
			sender.sendAsync(msg);
		
			Logger.trace("CDASender sent message");
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.trace("*** CDASender: Ending sendCDAMessage");
	}

}
