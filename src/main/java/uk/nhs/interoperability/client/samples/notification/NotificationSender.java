/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class NotificationSender.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class NotificationSender {

	/** The Constant NOTIFICATIONSTORE. */
	private static final String NOTIFICATIONSTORE = "urn:nhs-uk:addressing:ods:TESTORGS:NOTIFICATIONSTORE";

	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);

	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:R59:oncology";

	/** The Constant NOTIFICATION_SERVICENAME. */
	private static final String NOTIFICATION_SERVICENAME = "urn:nhs-itk:services:201005:SendEventNotification-v1-0";
	
	/** The Constant NOTIFICATION_PROFILEID. */
	private static final String NOTIFICATION_PROFILEID = "urn:nhs-en:profile:EventNotification-v1-0";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		NotificationSender appA = new NotificationSender();
		appA.sendNotificationMessage();
	
	}

	/**
	 * Send notification message.
	 */
	private void sendNotificationMessage(){
		Logger.trace("*** NotificationSender: Starting sendNotificationMessage");
		
		// Create the message
		ITKMessage msg = new SimpleMessage();
		Notification n = new Notification();
		n.setMessageId("NOTIFICATIONTEST");
		n.setEffectiveTime("20121127151500+0000");
		n.setDateOfBirth("20020831");
		n.setSenderOrg("Organisation1");
		n.setRecipientOrg("Organisation2");
		n.setRecipientAddress("Leeds, LS1 4HY");
		n.setPatientName("Joe Bloggs");
		n.setPatientAddress("Cleckheaton, BD19 3RH");
		n.setNHSNumber("1234556789");
		n.setEventTypeCode("01");
		n.setEventTypeDesc("DocumentEvent");
		n.setContactName("Fred Bloggs");
		n.setContactAddress("Bradford, BD1 1HY");
		n.setContactOrg("Organisation3");
		msg.setBusinessPayload(n.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(new ITKAddressImpl(NOTIFICATIONSTORE));
		mp.setServiceId(NOTIFICATION_SERVICENAME);
		//mp.setBusinessPayloadId(req.getMessageId());
		mp.setBusinessPayloadId("NOTIFICATION");
		mp.setProfileId(NOTIFICATION_PROFILEID);	// Not sure what the profile ID should be?

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			// Send this message asynchronously. The response message will be returned
			sender.sendAsync(msg);
		
			Logger.trace("Notification Sender sent message");
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
		}
		
		Logger.trace("*** Notification Sender: Ending sendNotificationMessage");
	}

}
