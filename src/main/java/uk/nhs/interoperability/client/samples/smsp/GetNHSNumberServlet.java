/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.smsp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;

/**
 * The Class GetNHSNumberServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class GetNHSNumberServlet extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new verify nhs number servlet.
     */
    public GetNHSNumberServlet() {
        super();
    }

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
	 * Process request.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("this is GetNHSNumberServlet.processRequest");
		String surname = request.getParameter("surname");
		String givenName = request.getParameter("givenName");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String gender = request.getParameter("gender");
		String postcode = request.getParameter("postcode");
		String localIdentifier = request.getParameter("localIdentifier");
		Logger.info("localIdentifier is:"+localIdentifier);
		String serviceAddress = request.getParameter("serviceAddress");
		ITKAddress itkServiceAddress = new ITKAddressImpl(serviceAddress);

        HttpSession session = request.getSession(true);
        session.removeAttribute("nhsNumber");
        session.removeAttribute("localIdentifier");
        session.removeAttribute("responseCode");
        session.removeAttribute("errorMessage");
        session.removeAttribute("responseMessage");
		
		SpineMiniServicesClient client = new SpineMiniServicesClient();
		
		GetNHSNumberRequest doc = new GetNHSNumberRequest();
		doc.givenName = givenName;
		doc.surname = surname;
		doc.dateOfBirth = dateOfBirth;
		doc.gender = gender;
		doc.postcode = postcode;
		doc.localIdentifier = localIdentifier;
		GetNHSNumberResponse resp = client.getNHSNumber(doc,itkServiceAddress);
		if (resp == null) {
			Logger.info("SMSP GNN FAILED - No response document");
            session.setAttribute("errorMessage", "Sorry this hasn't worked out this time. Please try again later.");
		} else {
			Logger.info("SMSP GNN COMPLETE. RESPONSE CODE:" + resp.getResponseCode());
			Logger.info("SMSP call succeeded");
			
	        //Obtain the session object, create a new session if doesn't exist
            session.setAttribute("nhsNumber", resp.getNhsNumber());
            session.setAttribute("localIdentifier", resp.getLocalIdentifier());
            session.setAttribute("responseCode", resp.getResponseCode());
			try {
				Document responseDoc = DomUtils.parse(resp.getPayload());
				String prettyPayload = DomUtils.serialiseToXML(responseDoc, DomUtils.PRETTY_PRINT);
 	            session.setAttribute("responseMessage", StringEscapeUtils.escapeHtml(prettyPayload));
			} catch (SAXException e) {
				Logger.error("SMSP GNN FAILED - SAXException",e);
	            session.setAttribute("errorMessage", "Sorry this really hasn't worked out this time. Please try again later.");
			} catch (ParserConfigurationException e) {
				Logger.error("SMSP GNN FAILED - ParserConfigurationException",e);
	            session.setAttribute("errorMessage", "Sorry this really hasn't worked out this time. Please try again later.");
			}
		}
		response.sendRedirect("./GetNHSNumberResponse.jsp");

	}

}
