/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples;

import uk.nhs.interoperability.consumer.AbstractCallbackListenerServlet;
import uk.nhs.interoperability.infrastructure.ITKAckDetails;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.source.ITKCallbackHandler;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class SimpleMessageCallbackHandler.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class SimpleMessageCallbackHandler extends AbstractCallbackListenerServlet implements ITKCallbackHandler {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -541728545823668074L;

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKCallbackHandler#onMessage(uk.nhs.interoperability.payload.ITKMessage)
	 */
	@Override
	public void onMessage(ITKMessage request) {
		Logger.trace("This is my example callback handler:onMessage()");
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKCallbackHandler#onAck(uk.nhs.interoperability.infrastructure.ITKAckDetails)
	 */
	@Override
	public void onAck(ITKAckDetails ack) {
		Logger.trace("Not implemented. Acks not expected for a Simple Message Pattern.");
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.source.ITKCallbackHandler#onNack(uk.nhs.interoperability.infrastructure.ITKAckDetails)
	 */
	@Override
	public void onNack(ITKAckDetails nack) {
		Logger.trace("Not implemented. Acks not expected for a Simple Message Pattern.");
	}

	/* (non-Javadoc)
	 * @see uk.nhs.interoperability.consumer.AbstractCallbackListenerServlet#getCallbackHandler()
	 */
	@Override
	public ITKCallbackHandler getCallbackHandler() {
		return this;
	}

}
