package uk.nhs.interoperability.client.samples;

import java.util.HashMap;

public class MessageStore {

	private static HashMap<String, String> messages = new HashMap<String, String>();

	public static void putMessage(String id, String message){
		messages.put(id,message);
		
	}
	public static String getMessage(String id){
		return messages.get(id);
	}

}
