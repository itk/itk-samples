/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.smsp;

import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;


/**
 * The Class GetPatientDetailsByNHSNumberResponse.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class GetPatientDetailsByNHSNumberResponse {

	/**
	 * Gets the nhs number.
	 *
	 * @return the nhs number
	 */
	public String getNhsNumber() {
		return nhsNumber;
	}
	
	/**
	 * Sets the nhs number.
	 *
	 * @param nhsNumber the new nhs number
	 */
	public void setNhsNumber(String nhsNumber) {
		this.nhsNumber = nhsNumber;
	}
	
	/**
	 * Gets the verified indicator.
	 *
	 * @return the verified indicator
	 */
	public boolean getVerifiedIndicator() {
		return verifiedIndicator;
	}
	
	/**
	 * Sets the verified indicator.
	 *
	 * @param verifiedIndicator the new verified indicator
	 */
	public void setVerifiedIndicator(boolean verifiedIndicator) {
		this.verifiedIndicator = verifiedIndicator;
	}
	
	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	public String getResponseCode() {
		return responseCode;
	}
	
	/**
	 * Sets the response code.
	 *
	 * @param responseCode the new response code
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	/**
	 * Gets the response message.
	 *
	 * @return the response message
	 */
	public String getPayload() {
		return responseMessage;
	}
	
	/**
	 * Sets the response message.
	 *
	 * @param responseMessage the new response message
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	// TODO: Change to appropriate response message
	/** The nhs number. */
	String nhsNumber;
	
	/** The verified indicator. */
	boolean verifiedIndicator;
	
	/** The response code. */
	String responseCode;
	
	/** The response message. */
	String responseMessage;

	/**
	 * Instantiates a new gets the patient details by nhs number response.
	 *
	 * @param response the response
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public GetPatientDetailsByNHSNumberResponse(ITKMessage response) throws ITKMessagingException {

		if (response==null || response.getBusinessPayload() == null){
			throw new ITKMessagingException("No payload returned from service");
		}

		// TODO : Build the response message from the response document
		//        For now just make one up.
		
		this.verifiedIndicator = true;
		this.nhsNumber = "1234567890";
		this.responseCode = "0000";
		this.responseMessage = response.getBusinessPayload();
		

	}	
	
}
