/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.smsp;

import java.io.IOException;
import java.io.Serializable;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.util.Logger;
import uk.nhs.interoperability.util.xml.DomUtils;
import uk.nhs.interoperability.util.xml.XPaths;


/**
 * The Class GetNHSNumberResponse.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class GetNHSNumberResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -510904168540955845L;
	/**
	 * Gets the nhs number.
	 *
	 * @return the nhs number
	 */
	public String getNhsNumber() {
		return nhsNumber;
	}
	
	/**
	 * Sets the nhs number.
	 *
	 * @param nhsNumber the new nhs number
	 */
	public void setNhsNumber(String nhsNumber) {
		this.nhsNumber = nhsNumber;
	}
		
	/**
	 * Gets the response code.
	 *
	 * @return the response code
	 */
	public String getResponseCode() {
		return responseCode;
	}
	
	/**
	 * Sets the response code.
	 *
	 * @param responseCode the new response code
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	/**
	 * Gets the response message.
	 *
	 * @return the response message
	 */
	public String getResponseMessage() {
		return responseMessage;
	}
	
	/**
	 * Sets the response message.
	 *
	 * @param responseMessage the new response message
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	/** The nhs number. */
	String nhsNumber;
	
	/** The local identifier. */
	String localIdentifier;
	
	public String getLocalIdentifier() {
		return localIdentifier;
	}

	public void setLocalIdentifier(String localIdentifier) {
		this.localIdentifier = localIdentifier;
	}

	/** The response code. */
	String responseCode;
	
	/** The response message. */
	String responseMessage;

	String payload;
	public String getPayload(){
		return payload;
	}
	
	/**
	 * Instantiates a new verify nhs number response.
	 *
	 * @param response the response
	 * @throws ITKMessagingException the iTK messaging exception
	 */
	public GetNHSNumberResponse(ITKMessage response) throws ITKMessagingException {

		if (response==null || response.getBusinessPayload() == null){
			throw new ITKMessagingException("No payload returned from service");
		}

		// Build The Response
		String nhsNumber="";
		String responseCode="";
		String localIdentifier="";
		try {
			Document responseDoc = DomUtils.parse(response.getBusinessPayload());
			responseCode = (String)XPaths.getXPathExpression(
					"/hl7:getNHSNumberResponse-v1-0/hl7:value/@code")
					.evaluate(responseDoc);
			nhsNumber = (String)XPaths.getXPathExpression(
					"/hl7:getNHSNumberResponse-v1-0/hl7:subject/hl7:patient/hl7:id[@root='2.16.840.1.113883.2.1.4.1']/@extension")
					.evaluate(responseDoc);
			localIdentifier = (String)XPaths.getXPathExpression(
					"/hl7:getNHSNumberResponse-v1-0/hl7:subject/hl7:patient/hl7:id[@root='2.16.840.1.113883.2.1.3.2.4.18.24']/@extension")
					.evaluate(responseDoc);
		} catch (SAXException e) {
			Logger.error("SAXException parsing GetNHSNumberResponse", e);
			throw new ITKMessagingException("SAXException parsing GetNHSNumberResponse");
		} catch (IOException e) {
			Logger.error("IOException parsing GetNHSNumberResponse", e);
			throw new ITKMessagingException("IOException parsing GetNHSNumberResponse");
		} catch (ParserConfigurationException e) {
			Logger.error("ParserConfigurationException parsing GetNHSNumberResponse", e);
			throw new ITKMessagingException("ParserConfigurationException parsing GetNHSNumberResponse");
		} catch (XPathExpressionException e) {
			Logger.error("XPathExpressionException parsing GetNHSNumberResponse", e);
			throw new ITKMessagingException("XPathExpressionException parsing GetNHSNumberResponse");
		}
					
		this.nhsNumber = nhsNumber;
		this.localIdentifier = localIdentifier;
		this.responseCode = responseCode;
		this.responseMessage = "";
		this.payload = response.getBusinessPayload();

	}
}
