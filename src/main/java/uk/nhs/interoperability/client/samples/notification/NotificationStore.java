/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import java.util.Date;
import java.util.HashMap;

import uk.nhs.interoperability.util.Logger;

/**
 * The Class NotificationStore.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class NotificationStore {

	/** The messages. */
	private static HashMap<String, String> messages = new HashMap<String, String>();
	
	/** The message dates. */
	private static HashMap<String, Date> messageDates = new HashMap <String, Date>(); 
	
	/**
	 * Put notification.
	 *
	 * @param id the id
	 * @param document the document
	 */
	public static void putNotification(String id, String document){
		Logger.trace("Adding notification to store, with ID = " + id);
		messages.put(id,document);
		messageDates.put(id, new Date());
	}

	/**
	 * Gets the notification.
	 *
	 * @param id the id
	 * @return the notification
	 */
	public static String getNotification(String id){
		return messages.get(id);
	}

	/**
	 * Gets the message list.
	 *
	 * @return the message list
	 */
	public static HashMap<String, Date> getMessageList() {
		return messageDates;
	}
}
