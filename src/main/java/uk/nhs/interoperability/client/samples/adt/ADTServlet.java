/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.adt;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class ADTServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class ADTServlet extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:TESTORGS:ORGA";

	/** The Constant SERVICE_PREFIX. */
	private static final String SERVICE_PREFIX = "urn:nhs-itk:services:201005:";
	
	/** The Constant ADT_PROFILEID. */
	private static final String ADT_PROFILEID = "ITKv1.0";
	       
    /**
     * Instantiates a new aDT servlet.
     */
    public ADTServlet() {
        super();
    }

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
	 * Process request.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("this is ADTServlet.processRequest");
		String docText = request.getParameter("docText");
		String serviceId = request.getParameter("serviceId");
		String serviceAddress = request.getParameter("serviceAddress");
		String responseType = request.getParameter("responseType");
		ITKAddress itkServiceAddress = new ITKAddressImpl(serviceAddress);

		HttpSession session = request.getSession(true);
        session.removeAttribute("errorMessage");
        session.removeAttribute("outcomeMessage");
        session.removeAttribute("responseMessage");
		
		// Create the message
		ITKMessage msg = new SimpleMessage();

		msg.setBusinessPayload(docText);
		
		UUID messageUUID = UUID.randomUUID();
		String messageId = messageUUID.toString();

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		
		// Example of sending another audit type
		//ITKIdentity ai = new ITKIdentityImpl("ANOTHER.AUDIT.ID","2.16.840.1.113883.0.0.1");
		//mp.setAuditIdentity(ai);

		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(itkServiceAddress);
		
		// Service is as requested on the web page
		mp.setServiceId(SERVICE_PREFIX+serviceId);
		mp.setBusinessPayloadId(messageId);

		// Profile Id is fixed (at present) for ADT
		mp.setProfileId(ADT_PROFILEID);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		String responseMsgTxt = "";
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			
			if (responseType.equalsIgnoreCase("SYNC")){
				
				// Send this message synchronously. The response message will be returned
				ITKMessage responseMsg = sender.sendSync(msg);
				responseMsgTxt = responseMsg.getBusinessPayload();
				responseMsgTxt = StringEscapeUtils.escapeHtml(responseMsgTxt);
		        session.setAttribute("responseMessage", responseMsgTxt);
		        session.setAttribute("outcomeMessage", "ADT Message Sent Successfully.");
			} else {
				// Send this message synchronously. The response message will be returned
				sender.sendAsync(msg);
		        session.setAttribute("outcomeMessage", "ADT Message Sent Asynchronously.");
				
			}
		
			Logger.trace(responseMsgTxt);
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
	        session.setAttribute("errorMessage", "Sorry this hasn't worked out this time. Please try again later.");
		}
		Logger.trace(responseMsgTxt);
		response.sendRedirect("./ADTResponse.jsp");

		Logger.info("*** ADTServlet: Ending.");
	}

}
