/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class NotificationSenderServlet.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class NotificationSenderServlet extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:R59:oncology";

	/** The Constant NOTIFICATION_SERVICENAME. */
	private static final String NOTIFICATION_SERVICENAME = "urn:nhs-itk:services:201005:SendEventNotification-v1-0";
	
	/** The Constant NOTIFICATION_PROFILEID. */
	private static final String NOTIFICATION_PROFILEID = "urn:nhs-en:profile:EventNotification-v1-0";

       
    /**
     * Instantiates a new notification sender servlet.
     */
    public NotificationSenderServlet() {
        super();
    }

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
	 * Process request.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Logger.debug("this is NotificationSenderServlet.processRequest");
		String messageId = request.getParameter("messageId");
		String nhsNumber = request.getParameter("nhsNumber");
		String dateOfBirth = request.getParameter("dateOfBirth");
		
		String senderOrg = request.getParameter("senderOrg");
		String recipientOrg = request.getParameter("recipientOrg");
		String recipientAddress = request.getParameter("recipientAddress");
		String patientName = request.getParameter("patientName");
		String patientAddress = request.getParameter("patientAddress");
		String contactName = request.getParameter("contactName");
		String contactAddress = request.getParameter("contactAddress");
		String contactOrg = request.getParameter("contactOrg");
		
		String businessAckRequired = request.getParameter("businessAckRequired");
		String serviceAddress = request.getParameter("serviceAddress");
		ITKAddress itkServiceAddress = new ITKAddressImpl(serviceAddress);

		HttpSession session = request.getSession(true);
        session.removeAttribute("errorMessage");
        session.removeAttribute("outcomeMessage");
        session.removeAttribute("responseMessage");
		
		Notification n = new Notification();
		n.setMessageId(messageId);
		n.setNHSNumber(nhsNumber);
		n.setDateOfBirth(dateOfBirth);
		
		n.setEffectiveTime("20121127151500+0000");
		n.setSenderOrg(senderOrg);
		n.setRecipientOrg(recipientOrg);
		n.setRecipientAddress(recipientAddress);
		n.setPatientName(patientName);
		n.setPatientAddress(patientAddress);
		
		n.setEventTypeCode("01");
		n.setEventTypeDesc("DocumentEvent");
		
		n.setContactName(contactName);
		n.setContactAddress(contactAddress);
		n.setContactOrg(contactOrg);
		
		// Create the message
		ITKMessage msg = new SimpleMessage();
		msg.setBusinessPayload(n.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(itkServiceAddress);
		mp.setServiceId(NOTIFICATION_SERVICENAME);
		mp.setBusinessPayloadId(n.getMessageId());
		mp.setProfileId(NOTIFICATION_PROFILEID);
		
		if (businessAckRequired.equalsIgnoreCase("Y")){
			mp.addHandlingSpecification(ITKMessageProperties.BUSINESS_ACK_HANDLING_SPECIFICATION_KEY, "true");
		}

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			// Send this message asynchronously. The response message will be returned
			sender.sendAsync(msg);
		
			Logger.info("Notification Message Sent.");
			session.setAttribute("outcomeMessage", "Notification Message Sent.");
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
	        session.setAttribute("errorMessage", "Sorry - I couldn't send the Notification this time.");
		}

		response.sendRedirect("./NotificationSent.jsp");
		
		Logger.trace("*** NotificationSenderServlet: Ending send");
	}

}
