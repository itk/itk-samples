/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.cda;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uk.nhs.interoperability.infrastructure.ITKAddress;
import uk.nhs.interoperability.infrastructure.ITKAddressImpl;
import uk.nhs.interoperability.infrastructure.ITKIdentity;
import uk.nhs.interoperability.infrastructure.ITKIdentityImpl;
import uk.nhs.interoperability.infrastructure.ITKMessageProperties;
import uk.nhs.interoperability.infrastructure.ITKMessagePropertiesImpl;
import uk.nhs.interoperability.infrastructure.ITKMessagingException;
import uk.nhs.interoperability.payload.ITKMessage;
import uk.nhs.interoperability.payload.SimpleMessage;
import uk.nhs.interoperability.source.ITKMessageSender;
import uk.nhs.interoperability.source.ITKMessageSenderImpl;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class HelloWorldCDASourceServlet.
 *
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 */
public class HelloWorldCDASourceServlet extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ORGID. */
	private static final String ORGID = "urn:nhs-uk:identity:ods:TESTORGS:ORGA";

	/** The Constant AUDITID. */
	private static final ITKIdentity AUDITID = new ITKIdentityImpl(ORGID);
	
	/** The Constant FROMADDRESS. */
	private static final String FROMADDRESS = "urn:nhs-uk:addressing:ods:R59:oncology";

	/** The Constant NON_CODED_CDA. */
	private static final String NON_CODED_CDA = "urn:nhs-itk:services:201005:SendCDADocument-v2-0";
	
	/** The Constant NCDA_PROFILEID. */
	private static final String NCDA_PROFILEID = "urn:nhs-en:profile:nonCodedCDADocument-v2-0";
       
    /**
     * Instantiates a new hello world cda source servlet.
     */
    public HelloWorldCDASourceServlet() {
        super();
    }

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
	 * Process request.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("this is HelloWorldCDASource.processRequest");
		String docId = request.getParameter("docId");
		String docText = request.getParameter("docText");
		String nhsNumber = request.getParameter("nhsNumber");
		String dateOfBirth = request.getParameter("dateOfBirth");
		String businessAckRequired = request.getParameter("businessAckRequired");
		String serviceAddress = request.getParameter("serviceAddress");
		ITKAddress itkServiceAddress = new ITKAddressImpl(serviceAddress);

		HttpSession session = request.getSession(true);
        session.removeAttribute("errorMessage");
        session.removeAttribute("outcomeMessage");
        session.removeAttribute("responseMessage");
		
		NonCodedCDA doc = new NonCodedCDA();
		doc.messageId = docId;
		doc.nhsNumber = nhsNumber;
		doc.dateOfBirth = dateOfBirth;
		doc.presentationText = docText;
		
		// Create the message
		ITKMessage msg = new SimpleMessage();
		msg.setBusinessPayload(doc.serialise());

		// Build the message properties.
		ITKMessageProperties mp = new ITKMessagePropertiesImpl();
		mp.setAuditIdentity(AUDITID);
		mp.setFromAddress(new ITKAddressImpl(FROMADDRESS));
		mp.setToAddress(itkServiceAddress);
		mp.setServiceId(NON_CODED_CDA);
		mp.setBusinessPayloadId(doc.getMessageId());
		mp.setProfileId(NCDA_PROFILEID);
		
		if (businessAckRequired.equalsIgnoreCase("Y")){
			mp.addHandlingSpecification(ITKMessageProperties.BUSINESS_ACK_HANDLING_SPECIFICATION_KEY, "true");
		}
		mp.addHandlingSpecification(ITKMessageProperties.INTERACTION_HANDLING_SPECIFICATION_KEY, 
				"urn:nhs-itk:interaction:primaryRecipientNonCodedCDADocument-v2-0");
		
		// Add the properties to the message
		msg.setMessageProperties(mp);

		// Add the properties to the message
		msg.setMessageProperties(mp);
		
		// Create the sender
		ITKMessageSender sender = new ITKMessageSenderImpl();

		try {
			// Send this message asynchronously. The response message will be returned
			sender.sendAsync(msg);
	        
			Logger.info("CDA Message Sent.");
			session.setAttribute("outcomeMessage", "CDA Message Sent.");
			
		} catch (ITKMessagingException e) {
			Logger.error("Error Sending ITK Message",e);
	        session.setAttribute("errorMessage", "Sorry - I couldn't send the message this time.");
		}

		response.sendRedirect("./CDASent.jsp");
		Logger.trace("*** HelloWorldCDASource: Ending send");
	}

}
