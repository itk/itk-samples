/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import winstone.Launcher;

/**
 * 
 * Creates a standalone server for the war - i.e.
 * provides the entry point for an executable
 * war file.<br/><br/>
 * 
 * For information on how to get this to work and
 * integration with Maven see:<br/><br/>
 * 
 * http://winstone.sourceforge.net/#embedding (for winstone server starting), and<br/>
 * http://internna.blogspot.co.uk/2011/08/step-by-step-executable-war-files.html
 * for details of maven integration (the example integration is with Jetty)
 *  
 * @author Michael Odling-Smee
 * @author Nicholas Jones
 * @since 0.1
 *
 */
public class StandaloneServer {

	/**
	 * Instantiates a new standalone server.
	 */
	private StandaloneServer() {		
	}
	
	/**
	 * Entry point for starting the Standalone Server
	 * from the executable war.
	 * 
	 * Reference to the executable war (itk-samples.war),
	 * note this means that you can only run the war
	 * from within the directory<br/><br/>
	 * 
	 * <code>java -jar itk-samples.war</code> will work<br/><br>
	 * 
	 * <code>java -jar path/to/war/itk-samples.war</code> won't work<br/><br>
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println("Starting standalone server... ");
		StandaloneServer standaloneServer = new StandaloneServer();
		standaloneServer.startUp(args);
	}
	
	private void startUp(String[] args) {
		Launcher winstone = null;
		try {
			Map<String, String> containerArgs = new HashMap<String, String>();	
			
			/*
			 * Tell it to use itself as the war file. The
			 * way this works is that Winstone (in the jar
			 * path in the itk-samples.war archive) starts
			 * and then loads a warfile - which happens to
			 * be the same archive
			 */
			containerArgs.put("warfile", "itk-samples.war");
			
			/*
			 * Need to provide a prefix so that configuration
			 * is consistent with when it is deployed as a 
			 * normal war within an appserver. If this prefix
			 * is changed then the routing configuration
			 * for callbacks etc. will have to be updated 
			 * accordingly
			 */
			containerArgs.put("prefix", "/itk-samples");
			
			/*
			 * Set the HTTP listener port
			 */
			containerArgs.put("httpPort", "8080");
			
			/*
			 * We ship with JSPs so need to indicate
			 * that Winstone should use Jasper
			 * to compile the JSPs
			 */
			containerArgs.put("useJasper", "true");
			
			/*
			 * Allow command line usage to override 
			 */
			for (int i = 0; i < args.length; i++) {
				String currentArg = args[i];
				//Check if this is a winstone style directive
				if (currentArg.startsWith("--")) {
					String key = currentArg.substring(2,  currentArg.indexOf("="));
					String value = currentArg.substring(currentArg.indexOf("=") + 1);
					containerArgs.put(key, value);
				}
			}
			
			/*
			 * Initialise the logging and launch
			 * Winstone
			 */
			Launcher.initLogger(containerArgs);
			winstone = new Launcher(containerArgs); 
			
			/*
			 * Need to block whilst Winstone
			 * is running
			 */
			while (winstone.isRunning()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					/*
					 * Shutdown winstone is the
					 * user presses Cntrl + C or
					 * similar
					 */
					winstone.shutdown(); 
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (winstone != null && winstone.isRunning()) {
				/*
				 * Belt and braces - to make sure winstone is
				 * shut down if any exception bubbles all the way
				 * to the top
				 */
				winstone.shutdown(); 
			}
		}
	}

}
