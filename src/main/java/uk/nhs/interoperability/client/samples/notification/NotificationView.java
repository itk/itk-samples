/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.client.samples.notification;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uk.nhs.interoperability.transform.TransformManager;
import uk.nhs.interoperability.util.Logger;

/**
 * The Class NotificationView.
 *
 * @author Adam Hatherly
 * @since 0.1
 */
public class NotificationView extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
       
    /**
     * Instantiates a new notification view.
     *
     * @see HttpServlet#HttpServlet()
     */
    public NotificationView() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Do get.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.debug("this is doGet");
		String notificationId= request.getParameter("notificationid");
		HttpSession session = request.getSession(true);
        session.removeAttribute("errorMessage");
        session.removeAttribute("outcomeMessage");
        session.removeAttribute("responseBody");
		
		if (null == notificationId) {
			session.setAttribute("outcomeMessage", "Notification List");
			session.setAttribute("responseBody", createList());
			
		} else {
			Logger.debug("Notification ID:"+notificationId);
			String htmlResponse = process(notificationId);
			Logger.trace(htmlResponse);
			if (htmlResponse.equals("NOTFOUND")){
				session.setAttribute("errorMessage", htmlResponse);
			} else {
				session.setAttribute("outcomeMessage", "Notification Found");
				session.setAttribute("responseBody", htmlResponse);
			}
		}
		
		response.sendRedirect("./NotificationView.jsp");
		Logger.trace("*** NotificationView: Ending");
	}

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Logger.debug("this is doPost");
		String docId= request.getParameter("docid");
		response.getWriter().write("DO POST. DOCID:"+docId);
	}
	
	
	/**
	 * Process.
	 *
	 * @param notificationId the notification id
	 * @return Content of notification, transformed to HTML
	 */
	protected String process(String notificationId) {
		String notification = NotificationStore.getNotification(notificationId);
		Logger.debug("Notification:"+notification);

		if (null!=notification){
			String htmlResponse = "";
			try {
				htmlResponse = TransformManager.doTransform("Notification_Renderer.xsl", notification);
			} catch (Exception e) {
				Logger.error("Notification Transformation Failed.",e);
			}

			Logger.trace(htmlResponse);
			return htmlResponse;
			
		} else {
			Logger.trace("Notification not found");
			return "NOTFOUND";
		}
	}
	
	/**
	 * Creates the list.
	 *
	 * @return the string
	 */
	protected String createList() {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String output = "";
		
		HashMap<String, Date> notifications = NotificationStore.getMessageList();
		if (!notifications.isEmpty()) {
			
			output = output + "<ul>";
			for (String key : notifications.keySet()) {
				String url = "./notificationview?notificationid=" + key;
				String date = formatter.format(notifications.get(key));
				output = output + "<li><a href=\"" + url + "\">ID: " + key + " - Received: " + date + "</a></li>";
			}
			output = output + "</ul>";
		} else {
			output = output + "<p>No Notifications Received</p>";
		}
		
		return output;
	}

}
